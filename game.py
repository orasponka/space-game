import pygame
import os
from threading import Timer
from random import randint
from random import randrange
from random import choice
import platform
pygame.init()
pygame.font.init()
pygame.mixer.init()

width, height = 1050, 650
window = pygame.display.set_mode((width, height))
pygame.display.set_caption("Space Game")

white = (255, 255, 255)
black = (0, 0, 0)
red_color = (255, 0, 0)
yellow_color = (255, 255, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
purple = (128, 0, 128)
gamemode_color = (135,206,235)
grey = (192,192,192)
orange = (252, 177, 3)

bullet_hit_sound = pygame.mixer.Sound(os.path.join('Sounds', 'hit.mp3'))
bullet_final_hit_sound = pygame.mixer.Sound(os.path.join('Sounds', 'final_hit.mp3'))
bullet_hit_asteroid = pygame.mixer.Sound(os.path.join('Sounds', 'bullet_hit_asteroid.wav'))
bullet_fire_sound = pygame.mixer.Sound(os.path.join('Sounds', 'fire.mp3'))
loot_collect_sound = pygame.mixer.Sound(os.path.join('Sounds', 'loot.wav'))
a_break_sound = pygame.mixer.Sound(os.path.join('Sounds', 'asteroid_break.wav'))
a_hit_sound = pygame.mixer.Sound(os.path.join('Sounds', 'asteroid_hit.wav'))
space_sound = pygame.mixer.Sound(os.path.join('Sounds', 'space.wav'))
granade_sound = pygame.mixer.Sound(os.path.join('Sounds', 'granade_explosion.ogg'))
button_sound = pygame.mixer.Sound(os.path.join('Sounds', 'button.wav'))
ball_sound = pygame.mixer.Sound(os.path.join('Sounds', 'ball-hit.wav'))
title_clickked_a_lot_times_sound = pygame.mixer.Sound(os.path.join('Sounds', 'soundeffect.wav'))
credits_music = pygame.mixer.Sound(os.path.join('Sounds', 'credits.mp3'))
skeleton_sound = pygame.mixer.Sound(os.path.join('Sounds', 'skeleton.mp3'))
skeleton2_sound = pygame.mixer.Sound(os.path.join('Sounds', 'skeleton2.wav'))

normal_volume = 0.75
button_sound.set_volume(0.8)
bullet_hit_asteroid.set_volume(1.5)
bullet_final_hit_sound.set_volume(1.25)
space_sound.set_volume(0.60)
bullet_hit_sound.set_volume(normal_volume - 0.20)
bullet_fire_sound.set_volume(normal_volume - 0.45)
loot_collect_sound.set_volume(normal_volume - 0.05)
a_hit_sound.set_volume(normal_volume)
ball_sound.set_volume(normal_volume - 0.10)
credits_music.set_volume(normal_volume)
title_clickked_a_lot_times_sound.set_volume(normal_volume)

font = pygame.font.get_default_font()
hp_font = pygame.font.SysFont(font, 40)
bullet_font = hp_font
winner_font = pygame.font.SysFont(font, 90)
menu_small_font = pygame.font.SysFont(font, 24)
menu_normal_font = pygame.font.SysFont(font, 50)
menu_fourty_font = pygame.font.SysFont(font, 40)
menu_title_font = pygame.font.Font(os.path.join('Fonts', 'Starjedi.ttf'), 100)
menu_title_smaller_font = pygame.font.Font(os.path.join('Fonts', 'Starjedi.ttf'), 80)
menu_medium_font = pygame.font.SysFont(font, 30)
coordinates_font = pygame.font.SysFont(font, 35)
shield_font = hp_font
tutorial_title_font = pygame.font.SysFont(font, 65)
tutorial_subtitle_font = pygame.font.SysFont(font, 40)
double_damage_font = hp_font
time_font = pygame.font.SysFont(font, 30)
controls_font = pygame.font.SysFont(font, 22)
class_font = pygame.font.SysFont(font, 32)
desc_font = pygame.font.SysFont(font, 27)
stat_font = pygame.font.SysFont(font, 23)

fps = 60
speed = width / 190
bullet_speed = width / 105.6
loot_speed = width / 475
bullets = 5
yellow_g_speed = width / 158
red_g_speed = width / -158
ba_width, ba_height = width // 17, width // 17
ball_speed = width // 75
ball_max = 20

gamemode = 1

difficulty_list = [
    [1, 6, 9, 16],
    [1, 8, 11, 19],
    [2, 10, 13, 22],
    [3, 12, 14, 25]
]

difficulty = difficulty_list[1]

hitboxes = False
a_img = True
a_menu = True
potato = False

max_granades = 3

border = pygame.Rect(width/2 - 2.5, 0, 5, height)

p_width, p_height = width / 17.3, height / 11
b_width, b_height = width // 86, height // 110
l_width, l_height = width // 63, width // 63
a_width, a_height = 90, 70
a_size_min, a_size_max = width / 17.3, width / 8.25
ball_y_speed, ball_x_speed = 0, 0

l_types = ["hp", "mspeed", "bspeed", "shield", "dd", "joker"]
l_max_time = 15
l_min_time = 5

default_hp = 12

player1_bullet_color = yellow_color
player2_bullet_color = red_color

single_player_survival = False

tournament_wins = 3

a_counter = False

particles_on = True
coordinates = False
show_classes = True
show_timer = True
spawn_loots = True
spawn_asteroids_boolean = True

yellow_hit = pygame.USEREVENT + 1
red_hit = pygame.USEREVENT + 2
loot_send = pygame.USEREVENT + 3
loot_get = pygame.USEREVENT + 4
yellow_hp_loot = pygame.USEREVENT + 5
red_hp_loot = pygame.USEREVENT + 6
yellow_b_loot = pygame.USEREVENT + 7
red_b_loot = pygame.USEREVENT + 8
yellow_m_loot = pygame.USEREVENT + 9
red_m_loot = pygame.USEREVENT + 10
red_s_loot = pygame.USEREVENT + 11
yellow_s_loot = pygame.USEREVENT + 12
red_sd = pygame.USEREVENT + 13
yellow_sd = pygame.USEREVENT + 14
yellow_hit_asteroid = pygame.USEREVENT + 15
red_hit_asteroid = pygame.USEREVENT + 16
asteroid_false = pygame.USEREVENT + 17
red_dd_loot = pygame.USEREVENT + 18
yellow_dd_loot = pygame.USEREVENT + 19
yellow_d_dd = pygame.USEREVENT + 20
red_d_dd = pygame.USEREVENT + 21
yellow_hit_g = pygame.USEREVENT + 22
red_hit_g = pygame.USEREVENT + 23
reg_send = pygame.USEREVENT + 24
challenger_send = pygame.USEREVENT + 25
open_settings = pygame.USEREVENT + 26
open_credits = pygame.USEREVENT + 27
yellow_d_asteroid = pygame.USEREVENT + 28
backmenu_e = pygame.USEREVENT + 29
quit_e = pygame.USEREVENT + 30
a_spawned = pygame.USEREVENT + 31
yellow_joker_loot = pygame.USEREVENT + 32
red_joker_loot = pygame.USEREVENT + 33
open_classes = pygame.USEREVENT + 34
open_tutorial = pygame.USEREVENT + 35

yellow_spaceship_image = pygame.image.load(os.path.join('Assets', 'yellow.png'))
yellow_spaceship = pygame.transform.rotate(pygame.transform.scale(yellow_spaceship_image, (p_width, p_height)), -90)
red_spaceship_image = pygame.image.load(os.path.join('Assets', 'red.png'))
red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90)

asteroid_image = pygame.image.load(os.path.join('Assets', 'asteroid.png'))

bg_image = pygame.transform.scale(pygame.image.load(os.path.join('Assets', 'background.jpg')), (width, height))


active_color = white
inactive_color = black
inputbox_bg = grey
class input_box:
    def __init__(self, x, y, w, h, font_size, text):
        self.rect = pygame.Rect(x, y, w, h)
        self.color = inactive_color
        self.text = text
        self.font_size = font_size
        self.textfont = pygame.font.SysFont(pygame.font.get_default_font(), self.font_size)
        self.txt_surface = self.textfont.render(text, True, self.color)
        self.active = False

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                if self.active == True:
                    self.active = False
                else:
                    self.active = True

            if self.active:
                self.color = active_color
            else:
                self.color = inactive_color

            self.txt_surface = self.textfont.render(self.text, True, self.color)
        if event.type == pygame.KEYDOWN:
            if self.active:
                if event.key == pygame.K_BACKSPACE:
                    self.text = self.text[:-1]
                else:
                    self.text += event.unicode
                self.txt_surface = self.textfont.render(self.text, True, self.color)

    def draw(self, window):
        pygame.draw.rect(window, inputbox_bg, self.rect)
        window.blit(self.txt_surface, (self.rect.x + 5, self.rect.y + self.txt_surface.get_height() / 2))

class text_button():
    def __init__(self, pos, w, h, text, click_function, click_function_args, font_size):
        self.color = white
        self.text_content = text
        self.font = pygame.font.SysFont(font, font_size)
        self.text = self.font.render(text, 1, self.color)
        self.x, self.y = pos
        self.width = w
        self.height = h
        self.rect = pygame.Rect(self.x, self.y, self.text.get_width(), self.text.get_height())
        self.click_func = click_function
        self.click_func_args = click_function_args

    def draw(self, screen):
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            self.text = self.font.render(self.text_content, 1, (180,180,180))
        else:
            self.text = self.font.render(self.text_content, 1, self.color)
        screen.blit(self.text, (self.x, self.y))

    def click(self, event):
        x, y = pygame.mouse.get_pos()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if pygame.mouse.get_pressed()[0]:
                if self.rect.collidepoint(x,y):
                    button_sound.play()
                    self.click_func(self.click_func_args)

buttoni = text_button((5,5), 50, 50, "Settings", pygame.event.post, pygame.event.Event(open_settings), 33)
buttoni2 = text_button((5,34), 50, 50, "Credits", pygame.event.post, pygame.event.Event(open_credits), 33)
buttoni3 = text_button((5, height - 57), 50, 50, "Classes", pygame.event.post, pygame.event.Event(open_classes), 33)
buttoni4 = text_button((5, height - 28), 50, 50, "Tutorial", pygame.event.post, pygame.event.Event(open_tutorial), 33)

class player_class():
    def __init__(self, name, desc, stats):
        self.name = name
        self.desc = desc
        self.stats = stats
        self.bspeed = stats[0]
        self.mspeed = stats[1]
        self.hp = stats[2]
        self.mines = stats[3]
        self.bullets = stats[4]
        self.ability = stats[5]

classes = [
    player_class("Tank", "+6 HP  -3 Movement Speed  +1 Bullet Speed  +1 Mine  -1 Bullet", [1, -3, 6, 1, -1, "Nothing"]),
    player_class("Sniper", "+3 Bullet Speed  -1 Bullet  -1 Mine  -1 HP", [3, 0, -1, -1, -1, "Nothing"]),
    player_class("Engineer", "+3 Mines  -2 Bullets  -1 Bullet speed  +1 Movement Speed  -2 HP", [-1, 1, -2, 3, -2, "Nothing"]),
    player_class("Assassin", "+3 Movement Speed  -1 Bullet  -1 Mine  -1 HP", [0, 3, -1, -1, -1, "Nothing"]),
    player_class("Lucky", "50% Double Damage Chance  -1 Mine  -2 Bullets  +1 Movement Speed  +1 Bullet Speed", [1, 1, 0, -1, -2, "50% Double Damage Chance"]),
    player_class("Mad", "-4 HP  +2 Movement Speed  +2 Bullet Speed  +1 Mine  -1 Bullet", [2, 2, -4, 1, -1, "Nothing"]),
    player_class("Healer", "Gain 1 HP every 10 seconds.  -3 HP  +2 Movement Speed  -1 Bullet  -1 Mine", [0, 2, -3, -1, -1, "Gain 1 HP every 10 seconds"]),
    player_class("Challenger", "Gain 1 Bullet Speed and 1 Movement Speed every 12 seconds.  -2 HP  -1 Movement Speed  -1 Bullet Speed  -1 Mine", [-1, -1, -2, -1, 0, "Gain 1 Bullet Speed and 1 Movement Speed every 12 seconds"]),
    player_class("Scout", "Immunity to asteroids  -3 HP  +1 Bullet Speed  +1 Movement Speed  -3 Mines  +1 Bullet", [1, 1, -3, -3, 1, "Immunity to asteroids"]),
    player_class("Vampire", "Gain 1 HP from every 2 hit to opponent.  -3 HP  -1 Bullet  -1 Mine  +1 Movement Speed  +1 Bullet Speed", [1, 1, -3, -1, -1, "Gain 1 HP from every 2 hit to opponent"]),
    player_class("None", "Default Settings", [0, 0, 0, 0, 0, "Nothing"]),
]
red_class = classes[-1].name
yellow_class = classes[-1].name
red_class_obj = classes[-1]
yellow_class_obj = classes[-1]

class granade():
    def __init__(self, x, y, size, x_vel, color, red):
        self.x = x
        self.y = y
        self.size = size
        self.rect = pygame.Rect(self.x, self.y, size, size)
        self.movetime = 70
        self.x_vel = x_vel
        self.removing = False
        self.color = color
        self.hp = 3
        self.red = red
    def remove(self, list):
        if self in list:
            list.remove(self)
    def update(self, list):
        if self.hp < 1:
            self.remove(list)
        if self in list:
            if self.movetime > 0:
                self.movetime -= 1
                if self.red:
                    self.x += self.x_vel - self.rect.width / 70
                else:
                    self.x += self.x_vel
                self.rect.x = self.x
                self.rect.y = self.y
                pygame.draw.rect(window, self.color, self.rect)
            else:
                pygame.draw.rect(window, self.color, self.rect)
                if self.removing == False:
                    self.removing = True
                    t = Timer(6, self.remove, [list]).start()

class asteroid_rect():
    def __init__(self, x, y, boolean, survival):
        a_size = randrange(int(a_size_min), int(a_size_max))
        a_width = a_size
        a_height = a_size / 1.2857
        self.w = a_width
        self.h = a_height
        self.asteroid = pygame.transform.scale(asteroid_image, (a_width, a_height))
        self.rect = pygame.Rect(self.asteroid.get_rect())
        self.rect.y = y
        if boolean:
            self.rect.x = x
        elif boolean == False:
            self.rect.x = x - self.rect.width
        if not survival:
            self.hp = randrange(1, 40)
        elif survival:
            self.hp = randrange(1, 30)
        if boolean:
            self.x_vel = randrange(width // -237, width // -width)
        elif boolean == False:
            self.x_vel = randrange(width // width,width // 237)
    def move(self, list):
        if self.rect.x > width + self.rect.width + 10 or self.rect.x < - self.rect.width - 10:
            list.remove(self)
        else:
            self.rect.x += self.x_vel

class loot_text():
    def __init__(self, x, y, size, color, text, player_1):
        self.x = x
        self.y = y
        self.font = pygame.font.SysFont(font, size)
        self.color = color
        self.lifetime = 40
        self.text = text
        self.y_vel = 1
        if player_1:
            self.x_vel = 1
        else:
            self.x_vel = -1
        self.obj = self.font.render(self.text, 1, self.color)
        self.player_1 = player_1
    def update(self, window, list):
        if self.lifetime > 0:
            self.lifetime -= 1
            self.x += self.x_vel
            self.y -= self.y_vel
            if self.player_1:
                window.blit(self.obj, (self.x, self.y))
            else:
                window.blit(self.obj, (self.x - self.obj.get_width(), self.y))
            pygame.display.update()
        else:
            list.pop(list.index(self))

class particle():
    def __init__(self, x, y, x_vel, y_vel, radius, color, gravity_scale):
        self.x = x
        self.y = y
        self.x_vel = x_vel
        self.y_vel = y_vel
        self.radius = radius
        self.color = color
        self.gravity_scale = gravity_scale * randrange(1, 2)
        self.lifetime = randrange(40, 100)
        self.gravity = randrange(5, 10)

    def draw(self, window):
        self.lifetime -= 1
        self.gravity -= self.gravity_scale
        self.x += self.x_vel * randrange(1,2)
        self.y += self.y_vel * self.gravity
        pygame.draw.circle(window, self.color, (self.x, self.y), self.radius)

def spawn_particles(particles, x, y):
    if particles_on:
        for i in range(randrange(8, 22)):
            particles.append(particle(x, y, randrange(-4, 4), randrange(-3, 0), choice([1, 1, 2]), white, 1))

def spawn_particles_asteroid(particles, x, y):
    if particles_on:
        grey = (0,0,0)
        if not potato:
            value = randrange(80,180)
            grey = (value, value, value)
        elif potato:
            grey = (218,165,32)
        for i in range(randrange(22, 56)):
            particles.append(particle(x, y, randrange(-6, 6), randrange(-4, 0), choice([1, 1, 1, 2, 3]), grey, 1))

def draw_particles(particles):
    for p in particles:
        if p.lifetime > 0:
            p.draw(window)
        else:
            particles.pop(particles.index(p))

def update_loot_texts(loot_texts):
    for t in loot_texts:
        t.update(window, loot_texts)

def draw_asteroids(asteroids, move):
    for a in asteroids:
        if move:
            a.move(asteroids)
        if a_img:
            window.blit(a.asteroid, (a.rect.x, a.rect.y))
        elif not a_img:
            pygame.draw.rect(window, (88,88,88), a.rect)

def handle_asteroids_tutorial(asteroids, yellow, yellow_bullets, particles):
    for a in asteroids:
        removed = False
        if yellow.colliderect(a.rect):
            asteroids.remove(a)
            spawn_particles_asteroid(particles, a.rect.x + a.rect.width / 2, a.rect.y + a.rect.height / 2)
            a_hit_sound.play()

        for bullet in yellow_bullets:
            if bullet.colliderect(a.rect):
                a.hp -= 1
                yellow_bullets.remove(bullet)
                if a.hp == 0:
                    asteroids.remove(a)
                    spawn_particles_asteroid(particles, a.rect.x + a.rect.width / 2, a.rect.y + a.rect.height / 2)
                    a_break_sound.play()
                else:
                    bullet_hit_asteroid.play()

def draw_tutorial(yellow, yellow_bullets, loots, lootTypes, particles, red_bullets, asteroids, red_granades):
    window.blit(bg_image, (0, 0))
    window.blit(yellow_spaceship, (yellow.x, yellow.y))

    draw_asteroids(asteroids, False)

    title = tutorial_title_font.render("Tutorial", 1, white)
    window.blit(title, (width/2 - title.get_width()/2, 10))

    hp_text = menu_small_font.render("Increase Health", 1, white)
    window.blit(hp_text, (950/5 - hp_text.get_width()/2 - 100, 100))

    move_text = menu_small_font.render("Increase Move Speed", 1, white)
    window.blit(move_text, (950/5 * 2 - move_text.get_width()/2 - 100, 100))

    bullet_text = menu_small_font.render("Increase Bullet Speed", 1, white)
    window.blit(bullet_text, (950/5 * 3 - bullet_text.get_width()/2 - 100, 100))

    joker_text = menu_small_font.render("Increase BSpeed, MSpeed and HP", 1, white)
    window.blit(joker_text, (950/5 * 3 - joker_text.get_width()/2 - 100, 310))

    shield_text = menu_small_font.render("Shield for 5s", 1, white)
    window.blit(shield_text, (950/5 * 4 - shield_text.get_width()/2 - 100, 100))

    dd_text = menu_small_font.render("Double Damage for 5s", 1, white)
    window.blit(dd_text, (950/5 * 5 - dd_text.get_width()/2 - 100, 100))

    other_text = controls_font.render("Other: ESC - Quit, F3 - Show Coordinates, F2 - Show Asteroid Counter", 1, white)
    window.blit(other_text, (10, height - other_text.get_height() -10))

    p2c_text = controls_font.render("Left Shift / Numpad 0 - Use Mine", 1, white)
    window.blit(p2c_text, (10, height - p2c_text.get_height() - other_text.get_height() - 20))

    p2c_2_text = controls_font.render("Player 2 Controls: Arrow Keys - Movement, Right CTRL / END / Numpad 0 - Shoot,", 1, white)
    window.blit(p2c_2_text, (10, height - p2c_2_text.get_height() - other_text.get_height() - p2c_text.get_height() - 30))

    p1c_text = controls_font.render("Player 1 Controls: WASD - Movement, F - Shoot, G - Use mine", 1, white)
    window.blit(p1c_text, (10, height - p1c_text.get_height() - p2c_text.get_height() - other_text.get_height() - p2c_2_text.get_height() - 40))

    b_text = menu_small_font.render("Hits 1HP", 1, white)
    window.blit(b_text, (697, 415))

    a_text = menu_small_font.render("Hits 3HP", 1, white)
    window.blit(a_text, (827, 415))

    g_text = menu_small_font.render("Hits 2HP", 1, white)
    window.blit(g_text, (827, 335))

    draw_particles(particles)



    for loot in loots:
        lootType = lootTypes[loots.index(loot)]
        color = (0,0,0)
        if lootType == "hp":
            color = red_color
        if lootType == "bspeed":
            color = green
        if lootType == "mspeed":
            color = yellow_color
        if lootType == "shield":
            color = blue
        if lootType == "dd":
            color = purple
        if lootType == "joker":
            color = orange

        pygame.draw.rect(window, color, loot)

    for bullet in yellow_bullets:
        pygame.draw.rect(window, player1_bullet_color, bullet)

    for bullet in red_bullets:
        pygame.draw.rect(window, player2_bullet_color, bullet)

    for g in red_granades:
        pygame.draw.rect(window, player2_bullet_color, g.rect)

    pygame.display.update()

def draw_menu(asteroids, turnaument, particles, ballmode, race):
    window.blit(bg_image, (0, 0))

    draw_asteroids(asteroids, False)

    buttoni.draw(window)
    buttoni2.draw(window)
    buttoni3.draw(window)
    buttoni4.draw(window)

    version_text = menu_small_font.render("v1.11.5", 1, white)
    window.blit(version_text, (width - version_text.get_width() - 5, height - version_text.get_height() - 5))

    #tutorial_text = menu_small_font.render("Press TAB to enter turorial", 1, white)
    #window.blit(tutorial_text, (0 + 5, height - tutorial_text.get_height() - 5))

    #class_text = menu_small_font.render("Press C to edit classes", 1, white)
    #window.blit(class_text, (0 + 5, height - tutorial_text.get_height() - class_text.get_height() - 10))

    title = menu_title_font.render("Space Game", 1, white)
    window.blit(title, (width/2 - title.get_width()/2, height/2 - title.get_height()/2))

    draw_text = menu_normal_font.render("Press space to start", 1, white)
    window.blit(draw_text, (width/2 - draw_text.get_width()/2, height/2 - draw_text.get_height()/2 + 100))

    draw_particles(particles)

    single_color = (0,0,0)
    turnaument_color = (0,0,0)
    ball_color = (0,0,0)
    race_color = (0,0,0)
    if turnaument:
        race_color = white
        single_color = white
        turnaument_color = gamemode_color
        ball_color = white
    elif not turnaument and not ballmode and not race:
        race_color = white
        single_color = gamemode_color
        turnaument_color = white
        ball_color = white
    elif ballmode:
        race_color = white
        single_color = white
        turnaument_color = white
        ball_color = gamemode_color
    elif race:
        race_color = gamemode_color
        single_color = white
        turnaument_color = white
        ball_color = white
    single_text = menu_medium_font.render("Single Game", 1, single_color)
    slash = menu_medium_font.render(" / ", 1, white)
    turnaument_text = menu_medium_font.render("Tournament", 1, turnaument_color)
    ball_text = menu_medium_font.render("Space Soccer", 1, ball_color)
    race_text = menu_medium_font.render("Survival", 1, race_color)

    window.blit(single_text, (width / 2 - single_text.get_width() - 170, height / 2 - single_text.get_height()/2 + 140))
    window.blit(slash, (width / 2 - single_text.get_width() - 30, height / 2 - slash.get_height()/2 + 140))
    window.blit(turnaument_text, (width / 2 - turnaument_text.get_width() - 10, height / 2 - turnaument_text.get_height()/2 + 140))
    window.blit(slash, (width / 2 + 5, height / 2 - slash.get_height()/2 + 140))
    window.blit(ball_text, (width/2 + 40, height/2 - ball_text.get_height()/2 + 140))
    window.blit(slash, (width / 2 + 50 + ball_text.get_width(), height / 2 - slash.get_height()/2 + 140))
    window.blit(race_text, (width / 2 + 75 + ball_text.get_width(), height / 2 - race_text.get_height()/2 + 140))

    pygame.display.update()

def draw_window(red, yellow, red_bullets, yellow_bullets, red_hp, yellow_hp, coordinates, loots, lootTypes, yellow_shield, red_shield, particles, loot_texts, asteroids, red_dd, yellow_dd, yellow_granades, red_granades, time, turnaument, red_wins, yellow_wins, yellow_c_bullets, red_c_bullets, yellow_c_mines, red_c_mines, ballmode, ball, race, asteroids_destroyed, asteroids_spawned):

    window.blit(bg_image, (0, 0))

    pygame.draw.rect(window, black, border)

    if ballmode:
        player1_target = pygame.Rect(0, 0, 2, height)
        player2_target = pygame.Rect(width - 2, 0, 2, height)
        pygame.draw.rect(window, player1_bullet_color, player1_target)
        pygame.draw.rect(window, player2_bullet_color, player2_target)

    for g in yellow_granades:
        g.update(yellow_granades)

    for g in red_granades:
        g.update(red_granades)

    draw_asteroids(asteroids, True)

    window.blit(yellow_spaceship, (yellow.x, yellow.y))
    window.blit(red_spaceship, (red.x, red.y))

    if ballmode:
        pygame.draw.rect(window, (255,165,0), ball)

    for bullet in red_bullets:
        pygame.draw.rect(window, player2_bullet_color, bullet)
    for bullet in yellow_bullets:
        pygame.draw.rect(window, player1_bullet_color, bullet)

    for loot in loots:
        lootType = lootTypes[loots.index(loot)]
        color = (0,0,0)
        if lootType == "hp":
            color = red_color
        if lootType == "bspeed":
            color = green
        if lootType == "mspeed":
            color = yellow_color
        if lootType == "shield":
            color = blue
        if lootType == "dd":
            color = purple
        if lootType == "joker":
            color = orange
        pygame.draw.rect(window, color, loot)

    if race and single_player_survival:
        yellow_hp_text = hp_font.render("Health: " + str(yellow_hp), 1, white)
        window.blit(yellow_hp_text, (10, 10))
    elif not ballmode:
        red_hp_text = hp_font.render("Health: " + str(red_hp), 1, white)
        yellow_hp_text = hp_font.render("Health: " + str(yellow_hp), 1, white)
        window.blit(red_hp_text, (width - red_hp_text.get_width() - 10, 10))
        window.blit(yellow_hp_text, (10, 10))

    if race and single_player_survival:
        yellow_bullet_text = bullet_font.render("Bullets: " + str(bullets - len(yellow_bullets) + yellow_c_bullets), 1, white)
        window.blit(yellow_bullet_text, (10, 50))
    elif not ballmode:
        red_bullet_text = bullet_font.render("Bullets: " + str(bullets - len(red_bullets) + red_c_bullets), 1, white)
        yellow_bullet_text = bullet_font.render("Bullets: " + str(bullets - len(yellow_bullets) + yellow_c_bullets), 1, white)
        window.blit(red_bullet_text, (width - red_bullet_text.get_width() - 10, 50))
        window.blit(yellow_bullet_text, (10, 50))
    elif ballmode:
        red_bullet_text = bullet_font.render("Bullets: " + str(bullets - len(red_bullets) + red_c_bullets), 1, white)
        yellow_bullet_text = bullet_font.render("Bullets: " + str(bullets - len(yellow_bullets) + yellow_c_bullets), 1, white)
        window.blit(red_bullet_text, (width - red_bullet_text.get_width() - 10, 10))
        window.blit(yellow_bullet_text, (10, 10))

    if not ballmode and not race:
        red_g_text = bullet_font.render("Mines: " + str(max_granades - len(red_granades) + red_c_mines), 1, white)
        yellow_g_text = bullet_font.render("Mines: " + str(max_granades - len(yellow_granades) + yellow_c_mines), 1, white)
        window.blit(red_g_text, (width - red_g_text.get_width() - 10, 90))
        window.blit(yellow_g_text, (10, 90))

    if race:
        selected_d = ""
        if difficulty == difficulty_list[0]:
            selected_d = "Easy"
        if difficulty == difficulty_list[1]:
            selected_d = "Normal"
        if difficulty == difficulty_list[2]:
            selected_d = "Hard"
        if difficulty == difficulty_list[3]:
            selected_d = "Very Hard"

        diffi_text = time_font.render(selected_d, 1, white)

        if single_player_survival:
            window.blit(diffi_text, (width - diffi_text.get_width() - 5, 5))
        elif not single_player_survival:
            padding = 0
            if show_timer:
                padding += 15
            if a_counter:
                padding += 15
            if padding == 15:
                padding -= 5
            if padding > 0:
                padding += diffi_text.get_height()
            elif padding == 0:
                padding += 5
            window.blit(diffi_text, (width/2 - diffi_text.get_width()/2, padding))


    minutes = str(int(time/60000)).zfill(2)
    seconds = str( int((time%60000)/1000) ).zfill(2)
    time_string = "%s:%s" % (minutes, seconds)
    time_text = time_font.render(time_string, 1, white)
    if show_timer:
        window.blit(time_text, (width / 2 - time_text.get_width() / 2, -12 + time_text.get_height()))

    if race and single_player_survival:
        a_text = time_font.render("{}".format(asteroids_destroyed), 1, white)
        if show_timer:
            window.blit(a_text, (width / 2 - a_text.get_width() / 2, 10 + a_text.get_height()))
        else:
            window.blit(a_text, (width / 2 - a_text.get_width() / 2, -12 + a_text.get_height()))

    if a_counter:
        a_counter_text = time_font.render("{}/{}".format(len(asteroids), asteroids_spawned), 1, white)
        if not race:
            window.blit(a_counter_text, (width/2 - a_counter_text.get_width()/2, height - 5 - a_counter_text.get_height()))
        elif race and single_player_survival:
            if race and show_timer:
                window.blit(a_counter_text, (width/2 - a_counter_text.get_width()/2, 33 + a_counter_text.get_height()))
            elif race and not show_timer:
                window.blit(a_counter_text, (width/2 - a_counter_text.get_width()/2, 10 + a_counter_text.get_height()))
        elif race and not single_player_survival:
            if race and show_timer:
                window.blit(a_counter_text, (width/2 - a_counter_text.get_width()/2, 10 + a_counter_text.get_height()))
            elif race and not show_timer:
                window.blit(a_counter_text, (width/2 - a_counter_text.get_width()/2, 5))


    if show_classes and not ballmode and not race:
        red_c_text = time_font.render(red_class, 1, white)
        window.blit(red_c_text, (width / 2 + time_text.get_width(), -12 + red_c_text.get_height()))

        yellow_c_text = time_font.render(yellow_class, 1, white)
        window.blit(yellow_c_text, (width / 2 - time_text.get_width() - yellow_c_text.get_width(), -12 + yellow_c_text.get_height()))

    if turnaument:
        wins_text = time_font.render(str(yellow_wins) + ":" + str(red_wins), 1, white)
        window.blit(wins_text, (width/2 - wins_text.get_width() / 2, 10 + wins_text.get_height()))

    draw_particles(particles)

    if yellow_shield:
        yellow_shield_text = shield_font.render("Shield", 1, white)
        window.blit(yellow_shield_text, (10, 130))

    if red_shield:
        red_shield_text = shield_font.render("Shield", 1, white)
        window.blit(red_shield_text, (width - red_shield_text.get_width() - 10, 130))

    if red_dd and not race:
        red_dd_text = double_damage_font.render("Double Damage", 1, white)
        window.blit(red_dd_text, (width- red_dd_text.get_width() - 10, 130))

    if yellow_dd and not race:
        yellow_dd_text = double_damage_font.render("Double Damage", 1, white)
        window.blit(yellow_dd_text, (10, 130))

    if coordinates:

        if race and single_player_survival:
            yellow_y_text = coordinates_font.render("y: " + str(yellow.y), 1, white)
            yellow_x_text = coordinates_font.render("x: " + str(yellow.x) + " ", 1, white)
            window.blit(yellow_x_text, (10, height - yellow_y_text.get_height() - 5))
            window.blit(yellow_y_text, (10 + yellow_x_text.get_width(), height - yellow_x_text.get_height() - 5))
        else:
            red_y_text = coordinates_font.render("y: " + str(red.y), 1, white)
            red_x_text = coordinates_font.render("x: " + str(red.x) + " ", 1, white)
            window.blit(red_y_text, (width - red_y_text.get_width() - 10, height - red_y_text.get_height() - 5))
            window.blit(red_x_text, (width - red_y_text.get_width() - red_x_text.get_width() - 10, height - red_x_text.get_height() - 5))

            yellow_y_text = coordinates_font.render("y: " + str(yellow.y), 1, white)
            yellow_x_text = coordinates_font.render("x: " + str(yellow.x) + " ", 1, white)
            window.blit(yellow_x_text, (10, height - yellow_y_text.get_height() - 5))
            window.blit(yellow_y_text, (10 + yellow_x_text.get_width(), height - yellow_x_text.get_height() - 5))

    update_loot_texts(loot_texts)

    pygame.display.update()

def yellow_handle_movement(keys_pressed, yellow, yellow_mspeed, game, yellow_c_mspeed):
    if game:
        if keys_pressed[pygame.K_a] and yellow.x - speed > 0:
            yellow.x -= speed + yellow_mspeed + yellow_c_mspeed
        if keys_pressed[pygame.K_d] and yellow.x + speed + yellow.width < border.x:
            yellow.x += speed + yellow_mspeed + yellow_c_mspeed
        if keys_pressed[pygame.K_w] and yellow.y - speed > 0:
            yellow.y -= speed + yellow_mspeed + yellow_c_mspeed
        if keys_pressed[pygame.K_s] and yellow.y + speed + yellow.height < height:
            yellow.y += speed + yellow_mspeed + yellow_c_mspeed
    elif game == False:
        if keys_pressed[pygame.K_a] and yellow.x - speed > 0:
            yellow.x -= speed + yellow_mspeed
        if keys_pressed[pygame.K_d] and yellow.x + speed + yellow.width < width:
            yellow.x += speed + yellow_mspeed
        if keys_pressed[pygame.K_w] and yellow.y - speed > 0:
            yellow.y -= speed + yellow_mspeed
        if keys_pressed[pygame.K_s] and yellow.y + speed + yellow.height < height:
            yellow.y += speed + yellow_mspeed

def red_handle_movement(keys_pressed, red, red_mspeed, red_c_mspeed, race):
    if not race:
        if keys_pressed[pygame.K_LEFT] and red.x - speed > border.x + border.width:
            red.x -= speed + red_mspeed + red_c_mspeed
        if keys_pressed[pygame.K_RIGHT] and red.x + speed + red.width < width:
            red.x += speed + red_mspeed + red_c_mspeed
        if keys_pressed[pygame.K_UP] and red.y - speed > 0:
            red.y -= speed + red_mspeed + red_c_mspeed
        if keys_pressed[pygame.K_DOWN] and red.y + speed + red.height < height:
            red.y += speed + red_mspeed + red_c_mspeed
    elif race:
        if keys_pressed[pygame.K_LEFT] and red.x - speed > 0:
            red.x -= speed + red_mspeed + red_c_mspeed
        if keys_pressed[pygame.K_RIGHT] and red.x + speed + red.width < border.x:
            red.x += speed + red_mspeed + red_c_mspeed
        if keys_pressed[pygame.K_UP] and red.y - speed > 0:
            red.y -= speed + red_mspeed + red_c_mspeed
        if keys_pressed[pygame.K_DOWN] and red.y + speed + red.height < height:
            red.y += speed + red_mspeed + red_c_mspeed

def handle_bullets_tutorial(yellow_bullets, yellow_bspeed, loots, lootTypes, particles, red_bullets, yellow, red_granades):
    for bullet in yellow_bullets:
        bullet.x += bullet_speed + yellow_bspeed

        if bullet.x > width:
            yellow_bullets.remove(bullet)

        for red_bullet in red_bullets:
            if red_bullet.colliderect(bullet):
                yellow_bullets.remove(bullet)
                red_bullets.remove(red_bullet)
                bullet_hit_sound.play()

        for loot in loots:
            if loot.colliderect(bullet):
                lootTypes.remove(lootTypes[loots.index(loot)])
                loots.remove(loot)
                yellow_bullets.remove(bullet)
                bullet_hit_sound.play()
                spawn_particles(particles, bullet.x + bullet.width, bullet.y + bullet.height / 2)

        for g in red_granades:
            if bullet.colliderect(g.rect):
                g.hp -= 1
                if g.hp < 1:
                    red_granades.remove(g)
                yellow_bullets.remove(bullet)
                bullet_hit_sound.play()

    for bullet in red_bullets:
        if bullet.colliderect(yellow):
            red_bullets.remove(bullet)
            bullet_hit_sound.play()

def handle_bullets(yellow_bullets, red_bullets, yellow, red, loots, lootTypes, yellow_bspeed, red_bspeed, particles, asteroids, red_dd, yellow_dd, yellow_granades, red_granades, yellow_c_bspeed, red_c_bspeed, red_c_ddc, yellow_c_ddc, ballmode, race):
    for bullet in yellow_bullets:
        removed = False
        bullet.x += bullet_speed + yellow_bspeed + yellow_c_bspeed
        if red.colliderect(bullet) and not ballmode and not race:
            pygame.event.post(pygame.event.Event(red_hit))
            spawn_particles(particles, bullet.x + bullet.width, bullet.y + bullet.height / 2)
            yellow_bullets.remove(bullet)
            removed = True
        elif bullet.x > width + 200:
            yellow_bullets.remove(bullet)
            removed = True

        for enemybullet in red_bullets:
            if bullet.colliderect(enemybullet) and removed == False and bullet in yellow_bullets and enemybullet in red_bullets:
                red_bullets.remove(enemybullet)
                removed == True
                yellow_bullets.remove(bullet)
                bullet_hit_sound.play()

        for loot in loots:
            if bullet.colliderect(loot) and removed == False:
                removed = True
                lootTypes.remove(lootTypes[loots.index(loot)])
                loots.remove(loot)
                spawn_particles(particles, bullet.x + bullet.width, bullet.y + bullet.height / 2)
                bullet_hit_sound.play()
                pygame.event.post(pygame.event.Event(loot_get))
                yellow_bullets.remove(bullet)

        for g in red_granades:
            if bullet.colliderect(g.rect) and removed == False:
                if yellow_dd:
                    g.hp -= 2
                elif yellow_c_ddc:
                    if randrange(1,2) == randint(1,2):
                        g.hp -= 2
                    else:
                        g.hp -= 1
                else:
                    g.hp -= 1

                if g.hp < 1:
                    red_granades.remove(g)
                bullet_hit_sound.play()
                removed = True
                yellow_bullets.remove(bullet)
                bullet_hit_sound.play()

        for g in yellow_granades:
            if bullet.colliderect(g.rect) and removed == False:
                if yellow_dd:
                    g.hp -= 2
                elif yellow_c_ddc:
                    if randrange(1,2) == randint(1,2):
                        g.hp -= 2
                    else:
                        g.hp -= 1
                else:
                    g.hp -= 1

                if g.hp < 1:
                    yellow_granades.remove(g)
                bullet_hit_sound.play()
                removed = True
                yellow_bullets.remove(bullet)
                bullet_hit_sound.play()

        for a in asteroids:
            damage = 1
            if yellow_dd:
                damage = 2
            elif yellow_c_ddc:
                if randrange(1,2) == randint(1,2):
                    damage = 2
                else:
                    damage = 1
            if a.rect.colliderect(bullet) and removed == False:
                if a.hp > 1:
                    if a.hp - damage < 1:
                        a.hp = 1
                    else:
                        a.hp -= damage
                    yellow_bullets.remove(bullet)
                    removed = True
                    bullet_hit_asteroid.play()
                elif a.hp == 1 or a.hp == 0:
                    removed = True
                    a_break_sound.play()
                    spawn_particles_asteroid(particles, a.rect.x + a.rect.width / 2, a.rect.y + a.rect.height / 2)
                    if race and single_player_survival:
                        pygame.event.post(pygame.event.Event(yellow_d_asteroid))
                    asteroids.remove(a)
                    yellow_bullets.remove(bullet)

    for bullet in red_bullets:
        removed = False
        if not race:
            bullet.x -= bullet_speed + red_bspeed + red_c_bspeed
        elif race:
            bullet.x += bullet_speed + red_bspeed + red_c_bspeed
        if yellow.colliderect(bullet) and not ballmode and not race:
            pygame.event.post(pygame.event.Event(yellow_hit))
            spawn_particles(particles, bullet.x + bullet.width, bullet.y + bullet.height / 2)
            red_bullets.remove(bullet)
            removed = True
        elif bullet.x < -200 and not race:
            red_bullets.remove(bullet)
            removed = True
        elif bullet.x > width + 200 and race:
            red_bullets.remove(bullet)
            removed = True

        for enemybullet in yellow_bullets:
            if bullet.colliderect(enemybullet) and removed == False and bullet in red_bullets and enemybullet in yellow_bullets:
                yellow_bullets.remove(enemybullet)
                red_bullets.remove(bullet)
                removed = True
                bullet_hit_sound.play()

        for loot in loots:
             if bullet.colliderect(loot) and removed == False:
                removed = True
                lootTypes.remove(lootTypes[loots.index(loot)])
                spawn_particles(particles, bullet.x + bullet.width, bullet.y + bullet.height / 2)
                loots.remove(loot)
                bullet_hit_sound.play()
                pygame.event.post(pygame.event.Event(loot_get))
                red_bullets.remove(bullet)

        for g in yellow_granades:
            if bullet.colliderect(g.rect) and removed == False:
                if red_dd:
                    g.hp -= 2
                elif red_c_ddc:
                    if randrange(1,2) == randint(1,2):
                        g.hp -= 2
                    else:
                        g.hp -= 1
                else:
                    g.hp -= 1

                if g.hp < 1:
                    yellow_granades.remove(g)
                bullet_hit_sound.play()
                removed = True
                red_bullets.remove(bullet)
                bullet_hit_sound.play()

        for g in red_granades:
            if bullet.colliderect(g.rect) and removed == False:
                if red_dd:
                    g.hp -= 2
                elif red_c_ddc:
                    if randrange(1,2) == randint(1,2):
                        g.hp -= 2
                    else:
                        g.hp -= 1
                else:
                    g.hp -= 1

                if g.hp < 1:
                    red_granades.remove(g)
                bullet_hit_sound.play()
                removed = True
                red_bullets.remove(bullet)
                bullet_hit_sound.play()

        for a in asteroids:
            damage = 1
            if red_dd:
                damage = 2
            elif red_c_ddc:
                if randrange(1,2) == randint(1,2):
                    damage = 2
                else:
                    damage = 1
            if a.rect.colliderect(bullet) and removed == False:
                if a.hp > 1:
                    removed = True
                    if a.hp - damage < 1:
                        a.hp = 1
                    else:
                        a.hp -= damage
                    red_bullets.remove(bullet)
                    bullet_hit_asteroid.play()
                elif a.hp == 1 or a.hp == 0:
                    removed = True
                    a_break_sound.play()
                    spawn_particles_asteroid(particles, a.rect.x + a.rect.width / 2, a.rect.y + a.rect.height / 2)
                    asteroids.remove(a)
                    red_bullets.remove(bullet)


def handle_asteroids_menu(asteroids):
    for a in asteroids:
        if a.rect.x < -200:
            a.rect.x = width + randrange(100, 500)
            a.rect.y = randint(0, height)
        a.rect.x -= 1

def handle_asteroids(asteroids, red, yellow, loots, lootTypes, particles, yellow_granades, red_granades):
    for a in asteroids:
        removed = False
        if a.rect.colliderect(red):
            spawn_particles_asteroid(particles, a.rect.x + a.rect.width / 2, a.rect.y + a.rect.height / 2)
            asteroids.remove(a)
            removed = True
            pygame.event.post(pygame.event.Event(red_hit_asteroid))
        if a.rect.colliderect(yellow):
            if removed == False:
                asteroids.remove(a)
                spawn_particles_asteroid(particles, a.rect.x + a.rect.width / 2, a.rect.y + a.rect.height / 2)
                removed = True
            pygame.event.post(pygame.event.Event(yellow_hit_asteroid))
        for loot in loots:
            if loot.colliderect(a.rect) and removed == False:
                lootTypes.remove(lootTypes[loots.index(loot)])
                loots.remove(loot)
                pygame.event.post(pygame.event.Event(loot_get))
        for g in red_granades:
            if a.rect.colliderect(g.rect):
                spawn_particles(particles, g.rect.x + g.rect.width / 2, g.rect.y + g.rect.height / 2)
                granade_sound.play()
                red_granades.remove(g)
                a.hp -= 1
                if a.hp < 1:
                    asteroids.remove(a)

        for g in yellow_granades:
            if a.rect.colliderect(g.rect):
                spawn_particles(particles, g.rect.x + g.rect.width / 2, g.rect.y + g.rect.height / 2)
                granade_sound.play()
                yellow_granades.remove(g)
                a.hp -= 1
                if a.hp < 1:
                    asteroids.remove(a)

def handle_loots_tutorial(loots, lootTypes, yellow):
    for loot in loots:
        if loot.colliderect(yellow):
            lootTypes.remove(lootTypes[loots.index(loot)])
            loots.remove(loot)
            loot_collect_sound.play()

def handle_loots(loots, red, yellow, lootTypes, loot_texts):
    for loot in loots:
        loot.y += loot_speed
        if loot.colliderect(yellow):
            if lootTypes[loots.index(loot)] == "hp":
                pygame.event.post(pygame.event.Event(yellow_hp_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, red_color, "+3 HP", True))
            if lootTypes[loots.index(loot)] == "mspeed":
                pygame.event.post(pygame.event.Event(yellow_m_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, yellow_color, "+2 Move Speed", True))
            if lootTypes[loots.index(loot)] == "bspeed":
                pygame.event.post(pygame.event.Event(yellow_b_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, green, "+2 Bullet Speed", True))
            if lootTypes[loots.index(loot)] == "shield":
                pygame.event.post(pygame.event.Event(yellow_s_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, blue, "Shield", True))
            if lootTypes[loots.index(loot)] == "dd":
                pygame.event.post(pygame.event.Event(yellow_dd_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, purple, "Double Damage", True))
            if lootTypes[loots.index(loot)] == "joker":
                pygame.event.post(pygame.event.Event(yellow_joker_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, orange, "+1 BSpeed, +1 MSpeed and +1 HP", True))
            lootTypes.remove(lootTypes[loots.index(loot)])
            loots.remove(loot)
            pygame.event.post(pygame.event.Event(loot_get))
        elif loot.colliderect(red):
            if lootTypes[loots.index(loot)] == "hp":
                pygame.event.post(pygame.event.Event(red_hp_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, red_color, "+3 HP", False))
            if lootTypes[loots.index(loot)] == "mspeed":
                pygame.event.post(pygame.event.Event(red_m_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, yellow_color, "+2 Move Speed", False))
            if lootTypes[loots.index(loot)] == "bspeed":
                pygame.event.post(pygame.event.Event(red_b_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, green, "+2 Bullet Speed", False))
            if lootTypes[loots.index(loot)] == "shield":
                pygame.event.post(pygame.event.Event(red_s_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, blue, "Shield", False))
            if lootTypes[loots.index(loot)] == "dd":
                pygame.event.post(pygame.event.Event(red_dd_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, purple, "Double Damage", False))
            if lootTypes[loots.index(loot)] == "joker":
                pygame.event.post(pygame.event.Event(red_joker_loot))
                loot_texts.append(loot_text(loot.x, loot.y, 25, orange, "+1 BSpeed, +1 MSpeed and +1 HP", False))
            lootTypes.remove(lootTypes[loots.index(loot)])
            loots.remove(loot)
            pygame.event.post(pygame.event.Event(loot_get))
        elif loot.y > height:
            lootTypes.remove(lootTypes[loots.index(loot)])
            loots.remove(loot)
            pygame.event.post(pygame.event.Event(loot_get))

def draw_winner(text, reason, reason2):
    draw_text = winner_font.render(text, 1, white)
    window.blit(draw_text, (width/2 - draw_text.get_width()/2, height/2 - draw_text.get_height()/2))
    reason_text = menu_medium_font.render(reason, 1, white)
    window.blit(reason_text, (width/2 - reason_text.get_width()/2, height/2 - reason_text.get_height()/2 + 50))
    reason2_text = menu_medium_font.render(reason2, 1, white)
    window.blit(reason2_text, (width/2 - reason2_text.get_width()/2, height/2 - reason2_text.get_height()/2 + 75))
    pygame.display.update()
    pygame.time.delay(4500)

def spawn_loot(loots, lootTypes):
    pygame.event.post(pygame.event.Event(loot_send))
    def spawn():
        loottype = randint(0, len(l_types) - 1)
        y = 0
        x = randint(10, width - 10)
        newloot = pygame.Rect(x, y, l_width, l_height)
        loots.append(newloot)
        lootTypes.append(l_types[loottype])
    delay = randint(l_min_time, l_max_time)
    t = Timer(delay, spawn)
    t.start()

def handle_particles(particles):
    for p in particles:
        if p.lifetime > 0:
            particle.draw(p, window)
        else:
            particles.pop(particles.index(p))

def handle_granades_tutorial(red_granades, yellow, particles):
    for g in red_granades:
        if g.rect.colliderect(yellow):
            red_granades.remove(g)
            spawn_particles(particles, g.rect.x + g.rect.width / 2, g.rect.y + g.rect.height / 2)
            granade_sound.play()

def tutorial():
    yellow = pygame.Rect(50, 325, p_height, p_width)
    yellow_bullets = []
    yellow_bspeed = 0
    loots = [
        pygame.Rect(82, 140, l_width, l_height),
        pygame.Rect(82, 175, l_width, l_height),
        pygame.Rect(82, 210, l_width, l_height),
        pygame.Rect(82, 245, l_width, l_height),
        pygame.Rect(270, 140, l_width, l_height),
        pygame.Rect(270, 175, l_width, l_height),
        pygame.Rect(270, 210, l_width, l_height),
        pygame.Rect(270, 245, l_width, l_height),
        pygame.Rect(455, 140, l_width, l_height),
        pygame.Rect(455, 175, l_width, l_height),
        pygame.Rect(455, 210, l_width, l_height),
        pygame.Rect(455, 245, l_width, l_height),
        pygame.Rect(650, 140, l_width, l_height),
        pygame.Rect(650, 175, l_width, l_height),
        pygame.Rect(650, 210, l_width, l_height),
        pygame.Rect(650, 245, l_width, l_height),
        pygame.Rect(840, 140, l_width, l_height),
        pygame.Rect(840, 175, l_width, l_height),
        pygame.Rect(840, 210, l_width, l_height),
        pygame.Rect(840, 245, l_width, l_height),
        pygame.Rect(455, 340, l_width, l_height),
        pygame.Rect(455, 375, l_width, l_height),
        pygame.Rect(455, 410, l_width, l_height),
        pygame.Rect(455, 445, l_width, l_height),

    ]
    lootTypes = [
        "hp", "hp", "hp", "hp",
        "mspeed", "mspeed", "mspeed", "mspeed",
        "bspeed", "bspeed", "bspeed", "bspeed",
        "shield", "shield", "shield", "shield",
        "dd", "dd", "dd", "dd",
        "joker", "joker", "joker", "joker"
    ]

    red_bullets = [
        pygame.Rect(725, 475, b_width, b_height)
    ]

    red_granades = [
        granade(850, 365, width // 55.88, 0, player2_bullet_color, True)
    ]

    asteroids = [
        asteroid_rect(815, 445, True, False)
    ]

    clock = pygame.time.Clock()
    run = True
    particles = []
    while run:
        clock.tick(fps)
        if space_sound.get_num_channels() < 1:
            space_sound.play()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    run = False
                    menu()
                if event.key == pygame.K_f and len(yellow_bullets) < bullets:
                    bullet = pygame.Rect(yellow.x + yellow.width, yellow.y + yellow.height // 2, b_width, b_height)
                    yellow_bullets.append(bullet)
                    bullet_fire_sound.play()
        if run:
            keys_pressed = pygame.key.get_pressed()
            handle_bullets_tutorial(yellow_bullets, yellow_bspeed, loots, lootTypes, particles, red_bullets, yellow, red_granades)
            handle_loots_tutorial(loots, lootTypes, yellow)
            handle_granades_tutorial(red_granades, yellow, particles)
            handle_asteroids_tutorial(asteroids, yellow, yellow_bullets, particles)
            yellow_handle_movement(keys_pressed, yellow, 0, False, 0)
            draw_tutorial(yellow, yellow_bullets, loots, lootTypes, particles, red_bullets, asteroids, red_granades)

def handle_class_change(player, up):
    global red_class, red_class_obj
    global yellow_class, yellow_class_obj
    if player == "red":
        if up == "up":
            new_class_index = 0
            for class_obj in classes:
                if class_obj.name == red_class:
                    new_class_index = classes.index(class_obj) - 1
            red_class = classes[new_class_index].name
            red_class_obj = classes[new_class_index]
        elif up == "down":
            new_class_index = 0
            for class_obj in classes:
                if class_obj.name == red_class:
                    new_class_index = classes.index(class_obj) + 1
                    if new_class_index > len(classes) - 1:
                        new_class_index = 0
            red_class = classes[new_class_index].name
            red_class_obj = classes[new_class_index]

    if player == "yellow":
        if up == "up":
            new_class_index = 0
            for class_obj in classes:
                if class_obj.name == yellow_class:
                    new_class_index = classes.index(class_obj) - 1
            yellow_class = classes[new_class_index].name
            yellow_class_obj = classes[new_class_index]
        elif up == "down":
            new_class_index = 0
            for class_obj in classes:
                if class_obj.name == yellow_class:
                    new_class_index = classes.index(class_obj) + 1
                    if new_class_index > len(classes) - 1:
                        new_class_index = 0
            yellow_class = classes[new_class_index].name
            yellow_class_obj = classes[new_class_index]


def draw_class_menu():
    window.blit(bg_image, (0, 0))

    yellow_text = menu_normal_font.render("Player 1: " + yellow_class, 1, white)
    window.blit(yellow_text, (width - yellow_text.get_width() - 10, 10))

    red_text = menu_normal_font.render("Player 2: " + red_class, 1, white)
    window.blit(red_text, (width - red_text.get_width() - 10, 20 + yellow_text.get_height()))

    padding = 55
    for class_object in classes:
        if class_object.name != "None":
            class_text = class_font.render(class_object.name, 1, white)
            window.blit(class_text, (10, -45 + padding))
            desc_text = desc_font.render(class_object.desc, 1, white)
            window.blit(desc_text, (10, -45 + class_text.get_height() + padding + 5))
            padding += 54

    yellow_stats_text = desc_font.render("Yellow Stats:", 1, white)
    red_stats_text = desc_font.render("Red Stats:", 1, white)
    window.blit(yellow_stats_text, (10, 550))
    window.blit(red_stats_text, (530, 550))

    y_padding = 10
    r_padding = 530

    y_hp_text = stat_font.render("HP: " + str(default_hp + yellow_class_obj.hp), 1, white)
    r_hp_text = stat_font.render("HP: " + str(default_hp + red_class_obj.hp), 1, white)
    window.blit(y_hp_text, (y_padding, 555 + 20))
    window.blit(r_hp_text, (r_padding, 555 + 20))

    y_padding += y_hp_text.get_width() + 10
    r_padding += r_hp_text.get_width() + 10

    y_bs_text = stat_font.render("Bullet Speed: " + str(round(bullet_speed, 0) + yellow_class_obj.bspeed), 1, white)
    r_bs_text = stat_font.render("Bullet Speed: " + str(round(bullet_speed, 0) + red_class_obj.bspeed), 1, white)
    window.blit(y_bs_text, (y_padding, 555 + 20))
    window.blit(r_bs_text, (r_padding, 555 + 20))


    y_padding += y_bs_text.get_width() + 10
    r_padding += r_bs_text.get_width() + 10

    y_ms_text = stat_font.render("Movement Speed: " + str(round(speed, 0) + yellow_class_obj.mspeed), 1, white)
    r_ms_text = stat_font.render("Movement Speed: " + str(round(speed, 0) + red_class_obj.mspeed), 1, white)
    window.blit(y_ms_text, (y_padding, 555 + 20))
    window.blit(r_ms_text, (r_padding, 555 + 20))

    y_padding = 10
    r_padding = 530

    y_mine_text = stat_font.render("Mines: " + str(max_granades + yellow_class_obj.mines), 1, white)
    r_mine_text = stat_font.render("Mines: " + str(max_granades + red_class_obj.mines), 1, white)
    window.blit(y_mine_text, (y_padding, 555 + 40))
    window.blit(r_mine_text, (r_padding, 555 + 40))

    y_padding += r_mine_text.get_width() + 10
    r_padding += r_mine_text.get_width() + 10

    y_bullet_text = stat_font.render("Bullets: " + str(bullets + yellow_class_obj.bullets), 1, white)
    r_bullet_text = stat_font.render("Bullets: " + str(bullets + red_class_obj.bullets), 1, white)
    window.blit(y_bullet_text, (y_padding, 555 + 40))
    window.blit(r_bullet_text, (r_padding, 555 + 40))

    y_padding = 10
    r_padding = 530

    y_a_text = stat_font.render("Ability: " + yellow_class_obj.ability, 1, white)
    r_a_text = stat_font.render("Ability: " + red_class_obj.ability, 1, white)
    window.blit(y_a_text, (y_padding, 555 + 60))
    window.blit(r_a_text, (r_padding, 555 + 60))

    pygame.display.update()

def class_menu():
    run = True
    clock = pygame.time.Clock()
    while run:
        clock.tick(fps)
        if space_sound.get_num_channels() < 1:
            space_sound.play()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    run = False
                if event.key == pygame.K_UP:
                    handle_class_change("red", "up")
                if event.key == pygame.K_DOWN:
                    handle_class_change("red", "down")
                if event.key == pygame.K_w:
                    handle_class_change("yellow", "up")
                if event.key == pygame.K_s:
                    handle_class_change("yellow", "down")
        draw_class_menu()
    menu()

display = False
gameplay = False
appearance = False
audio = False
other = False
winmode = (width, height)
custom_w = input_box(210, 600, 140, 35, 28, "1920")
custom_h = input_box(410, 600, 140, 35, 28, "1080")
def settings():
    global gameplay, display, appearance, audio, other, custom_h, custom_w
    def change_tab(tab):
        global gameplay
        global display
        global appearance
        global audio
        global other
        if tab == "gameplay":
            gameplay = True
            display = False
            appearance = False
            audio = False
            other = False
        elif tab == "display":
            gameplay = False
            display = True
            appearance = False
            audio = False
            other = False
        elif tab == "appearance":
            gameplay = False
            display = False
            appearance = True
            audio = False
            other = False
        elif tab == "audio":
            audio = True
            gameplay = False
            display = False
            appearance = False
            other = False
        elif tab == "other":
            other = True
            gameplay = False
            display = False
            appearance = False
            audio = False

    settings_exit = pygame.USEREVENT + 1349
    buttons = [
        text_button((5, 10), 100, 50, "Display", change_tab, "display", 35),
        text_button((5, 45), 100, 50, "Gameplay", change_tab, "gameplay", 35),
        text_button((5, 80), 100, 50, "Appearance", change_tab, "appearance", 35),
        text_button((5, 115), 100, 50, "Audio", change_tab, "audio", 35),
        text_button((5, 150), 100, 50, "Other", change_tab, "other", 35),
        text_button((5, height - 30), 100, 50, "Exit to menu", pygame.event.post, pygame.event.Event(settings_exit), 35)
    ]

    def set_mode(size):
        global width
        global height
        global window
        global border
        global bg_image
        global winmode
        global p_width, p_height
        global red_spaceship, yellow_spaceship
        global a_size_min, a_size_max
        global l_width, l_height
        global b_width, b_height
        global yellow_g_speed, red_g_speed, loot_speed, bullet_speed
        global ball_speed, ba_height, ba_width
        global speed
        global buttoni3, buttoni4
        width = size[0]
        height = size[1]
        speed = width / 190
        winmode = (width, height)
        bullet_speed = width / 105.6
        loot_speed = width / 475
        yellow_g_speed = width / 158
        red_g_speed = width / -158
        p_width, p_height = width / 17.3, height / 11
        a_size_min, a_size_max = width / 17.3, width / 8.25
        l_width, l_height = width // 63, width // 63
        b_width, b_height = width // 86, height // 110
        window = pygame.display.set_mode((width, height))
        bg_image = pygame.transform.scale(pygame.image.load(os.path.join('Assets', 'background.jpg')), (width, height))
        buttons[-1] = text_button((5, height - 30), 100, 50, "Exit to menu", pygame.event.post, pygame.event.Event(settings_exit), 35)
        border = pygame.Rect(width/2 - 2.5, 0, 5, height)
        yellow_spaceship = pygame.transform.rotate(pygame.transform.scale(yellow_spaceship_image, (p_width, p_height)), -90)
        red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90)
        ba_width, ba_height = width // 17, width // 17
        ball_speed = width // 75
        buttoni3 = text_button((5, height - 57), 50, 50, "Classes", pygame.event.post, pygame.event.Event(open_classes), 33)
        buttoni4 = text_button((5, height - 28), 50, 50, "Tutorial", pygame.event.post, pygame.event.Event(open_tutorial), 33)

    def set_custom_mode(nothing):
        global custom_w, custom_h
        global width
        global height
        global window
        global border
        global bg_image
        global winmode
        global p_width, p_height
        global red_spaceship, yellow_spaceship
        global a_size_min, a_size_max
        global l_width, l_height
        global b_width, b_height
        global yellow_g_speed, red_g_speed, loot_speed, bullet_speed
        global ball_speed, ba_height, ba_width
        global speed
        global buttoni3, buttoni4
        try:
            width = int(custom_w.text)
            height = int(custom_h.text)
            speed = width / 190
            winmode = (width, height)
            bullet_speed = width / 105.6
            loot_speed = width / 475
            yellow_g_speed = width / 158
            red_g_speed = width / -158
            p_width, p_height = width / 17.3, height / 11
            a_size_min, a_size_max = width / 17.3, width / 8.25
            l_width, l_height = width // 63, width // 63
            b_width, b_height = width // 86, height // 110
            window = pygame.display.set_mode((width, height))
            bg_image = pygame.transform.scale(pygame.image.load(os.path.join('Assets', 'background.jpg')), (width, height))
            buttons[-1] = text_button((5, height - 30), 100, 50, "Exit to menu", pygame.event.post, pygame.event.Event(settings_exit), 35)
            border = pygame.Rect(width/2 - 2.5, 0, 5, height)
            yellow_spaceship = pygame.transform.rotate(pygame.transform.scale(yellow_spaceship_image, (p_width, p_height)), -90)
            red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90)
            ba_width, ba_height = width // 17, width // 17
            ball_speed = width // 75
            buttoni3 = text_button((5, height - 57), 50, 50, "Classes", pygame.event.post, pygame.event.Event(open_classes), 33)
            buttoni4 = text_button((5, height - 28), 50, 50, "Tutorial", pygame.event.post, pygame.event.Event(open_tutorial), 33)
        except ValueError:
            print("Can't convert text field texts into int")


    def toggle_fullscreen(nothing):
        global width, height
        global window
        window = pygame.display.set_mode((width, height), pygame.FULLSCREEN)

    def toggle_fullscreen_mode(size):
        global width, height
        global window
        global border
        global bg_image
        global p_width, p_height
        global red_spaceship, yellow_spaceship
        global a_size_min, a_size_max
        global l_width, l_height
        global b_width, b_height
        global yellow_g_speed, red_g_speed, loot_speed, bullet_speed
        global ball_speed, ba_height, ba_width
        global speed
        global buttoni3, buttoni4
        width, height = size[0], size[1]
        bullet_speed = width / 105.6
        loot_speed = width / 475
        yellow_g_speed = width / 158
        red_g_speed = width / -158
        p_width, p_height = width / 17.3, height / 11
        a_size_min, a_size_max = width / 17.3, width / 8.25
        l_width, l_height = width // 63, width // 63
        b_width, b_height = width // 86, height // 110
        window = pygame.display.set_mode((width, height), pygame.FULLSCREEN)
        bg_image = pygame.transform.scale(pygame.image.load(os.path.join('Assets', 'background.jpg')), (width, height))
        buttons[-1] = text_button((5, height - 30), 100, 50, "Exit to menu", pygame.event.post, pygame.event.Event(settings_exit), 35)
        border = pygame.Rect(width/2 - 2.5, 0, 5, height)
        yellow_spaceship = pygame.transform.rotate(pygame.transform.scale(yellow_spaceship_image, (p_width, p_height)), -90)
        red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90)
        ba_width, ba_height = width // 17, width // 17
        ball_speed = width // 75
        speed = width / 190
        buttoni3 = text_button((5, height - 57), 50, 50, "Classes", pygame.event.post, pygame.event.Event(open_classes), 33)
        buttoni4 = text_button((5, height - 28), 50, 50, "Tutorial", pygame.event.post, pygame.event.Event(open_tutorial), 33)


    def escape_fullscreen(nothing):
        global speed
        global width
        global height
        global window
        global border
        global winmode
        global bg_image
        global p_width, p_height
        global red_spaceship, yellow_spaceship
        global a_size_min, a_size_max
        global l_width, l_height
        global b_width, b_height
        global yellow_g_speed, red_g_speed, loot_speed, bullet_speed
        global ball_speed, ba_height, ba_width
        global buttoni3, buttoni4
        width, height = winmode
        bullet_speed = width / 105.6
        loot_speed = width / 475
        yellow_g_speed = width / 158
        red_g_speed = width / -158
        p_width, p_height = width / 17.3, height / 11
        a_size_min, a_size_max = width / 17.3, width / 8.25
        l_width, l_height = width // 63, width // 63
        b_width, b_height = width // 86, height // 110
        window = pygame.display.set_mode((width, height))
        bg_image = pygame.transform.scale(pygame.image.load(os.path.join('Assets', 'background.jpg')), (width, height))
        buttons[-1] = text_button((5, height - 30), 100, 50, "Exit to menu", pygame.event.post, pygame.event.Event(settings_exit), 35)
        border = pygame.Rect(width/2 - 2.5, 0, 5, height)
        yellow_spaceship = pygame.transform.rotate(pygame.transform.scale(yellow_spaceship_image, (p_width, p_height)), -90)
        red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90)
        width, height = winmode
        p_width, p_height = width / 17.3, height / 11
        a_size_min, a_size_max = width / 17.3, width / 8.25
        l_width, l_height = width // 63, width // 63
        b_width, b_height = width // 86, height // 110
        window = pygame.display.set_mode((width, height))
        bg_image = pygame.transform.scale(pygame.image.load(os.path.join('Assets', 'background.jpg')), (width, height))
        buttons[-1] = text_button((5, height - 30), 100, 50, "Exit to menu", pygame.event.post, pygame.event.Event(settings_exit), 35)
        border = pygame.Rect(width/2 - 2.5, 0, 5, height)
        yellow_spaceship = pygame.transform.rotate(pygame.transform.scale(yellow_spaceship_image, (p_width, p_height)), -90)
        red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90)
        ba_width, ba_height = width // 17, width // 17
        ball_speed = width // 75
        speed = width / 190
        buttoni3 = text_button((5, height - 57), 50, 50, "Classes", pygame.event.post, pygame.event.Event(open_classes), 33)
        buttoni4 = text_button((5, height - 28), 50, 50, "Tutorial", pygame.event.post, pygame.event.Event(open_tutorial), 33)

    display_btns = [
        text_button((210, 50), 100, 50, "1400x1000 mode", set_mode, [1400, 1000], 35),
        text_button((210, 80), 100, 50, "1200x800 mode", set_mode, [1200, 800], 35),
        text_button((210, 110), 100, 50, "1100x700 mode", set_mode, [1100, 700], 35),
        text_button((210, 140), 100, 50, "1050x650 mode (Default) (Lower not recommended)", set_mode, [1050, 650], 35),
        text_button((210, 170), 100, 50, "1000x600 mode", set_mode, [1000, 600], 35),
        text_button((210, 200), 100, 50, "950x550 mode", set_mode, [950, 550], 35),
        text_button((210, 230), 100, 50, "900x500 mode", set_mode, [900, 500], 35),
        text_button((210, 260), 100, 50, "850x450 mode", set_mode, [850,450], 35),
        text_button((210, 300), 100, 50, "Fullscreen mode on current window size", toggle_fullscreen, "", 35),
        text_button((210, 330), 100, 50, "Fullscreen mode on 1280x720 (HD / 720p)", toggle_fullscreen_mode, [1280, 720], 35),
        text_button((210, 360), 100, 50, "Fullscreen mode on 1366x768 (HD / 1,049K)", toggle_fullscreen_mode, [1366, 768], 35),
        text_button((210, 390), 100, 50, "Fullscreen mode on 1280x1024 (SXGA)", toggle_fullscreen_mode, [1280, 1024], 35),
        text_button((210, 420), 100, 50, "Fullscreen mode on 1400x1050 (SXGA+)", toggle_fullscreen_mode, [1400, 1050], 35),
        text_button((210, 450), 100, 50, "Fullscreen mode on 1920x1080 (Full HD / 1080p)", toggle_fullscreen_mode, [1920, 1080], 35),
        text_button((210, 480), 100, 50, "Fullscreen mode on 2560x1440 (Quad HD / 1440p / 2K)", toggle_fullscreen_mode, [2560, 1440], 35),
        text_button((210, 510), 100, 50, "Fullscreen mode on 3840x2160 (Ultra HD / 4K)", toggle_fullscreen_mode, [3840, 2160], 35),
        text_button((210, 540), 100, 50, "Escape fullscreen", escape_fullscreen, "", 35),
    ]

    def set_t_wins(wins):
        global tournament_wins
        tournament_wins = wins

    t_btns = [
        text_button((210, 90), 100, 50, "2 (Best of three matches)", set_t_wins, 2, 35),
        text_button((210, 120), 100, 50, "3 (Best of five matches) (Default)", set_t_wins, 3, 35),
        text_button((210, 150), 100, 50, "4 (Best of seven matches)", set_t_wins, 4, 35),
        text_button((210, 180), 100, 50, "5 (Best of nine matches)", set_t_wins, 5, 35),
        text_button((210, 210), 100, 50, "6 (Best of eleven matches)", set_t_wins, 6, 35),
    ]

    def set_particles(p):
        global particles_on
        if p:
            particles_on = True
        elif not p:
            particles_on = False

    p_btns = [
        text_button((210,90), 100, 50, "On (Default)", set_particles, True, 35),
        text_button((210,120), 100, 50, "Off", set_particles, False, 35),
    ]

    def set_p1_color(color):
        global player1_bullet_color
        global yellow_spaceship_image
        global yellow_spaceship
        global p_width, p_height

        yellow_spaceship_image = pygame.image.load(os.path.join('Assets', '{}.png'.format(color)))
        yellow_spaceship = pygame.transform.rotate(pygame.transform.scale(yellow_spaceship_image, (p_width, p_height)), -90)

        if color == "red":
            player1_bullet_color = red_color
        elif color == "yellow":
            player1_bullet_color = yellow_color
        elif color == "blue":
            player1_bullet_color = blue
        elif color == "green":
            player1_bullet_color = green
        elif color == "purple":
            player1_bullet_color = purple

    def set_p2_color(color):
        global player2_bullet_color
        global red_spaceship_image
        global red_spaceship
        global p_width, p_height

        red_spaceship_image = pygame.image.load(os.path.join('Assets', '{}.png'.format(color)))
        red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90)

        if color == "red":
            player2_bullet_color = red_color
        elif color == "yellow":
            player2_bullet_color = yellow_color
        elif color == "blue":
            player2_bullet_color = blue
        elif color == "green":
            player2_bullet_color = green
        elif color == "purple":
            player2_bullet_color = purple

    player1_btns = [
        text_button((210, 195), 100, 50, "Red", set_p1_color, "red", 35),
        text_button((210, 230), 100, 50, "Yellow (Default)", set_p1_color, "yellow", 35),
        text_button((210, 265), 100, 50, "Blue", set_p1_color, "blue", 35),
        text_button((210, 300), 100, 50, "Green", set_p1_color, "green", 35),
        text_button((210, 335), 100, 50, "Purple", set_p1_color, "purple", 35),
    ]

    player2_btns = [
        text_button((210, 405), 100, 50, "Red (Default)", set_p2_color, "red", 35),
        text_button((210, 440), 100, 50, "Yellow", set_p2_color, "yellow", 35),
        text_button((210, 475), 100, 50, "Blue", set_p2_color, "blue", 35),
        text_button((210, 510), 100, 50, "Green", set_p2_color, "green", 35),
        text_button((210, 545), 100, 50, "Purple", set_p2_color, "purple", 35),
    ]

    def set_g_color(color):
        global gamemode_color
        gamemode_color = color

    gamemodecolor_btns = [
        text_button((510, 195), 100, 50, "Red", set_g_color, red_color, 35),
        text_button((510, 230), 100, 50, "Orange", set_g_color, (255,165,0), 35),
        text_button((510, 265), 100, 50, "Sky Blue (Default)", set_g_color, (135,206,235), 35),
        text_button((510, 300), 100, 50, "Magenta", set_g_color, (255,0,255), 35),
        text_button((510, 335), 100, 50, "Forest Green", set_g_color, (34,139,34), 35),
    ]


    def set_hp(hp):
        global default_hp
        default_hp = hp

    a_btns = [
        text_button((210, 275), 100, 50, "10 HP", set_hp, 10, 35),
        text_button((210, 305), 100, 50, "12 HP (Default)", set_hp, 12, 35),
        text_button((210, 335), 100, 50, "15 HP", set_hp, 15, 35),
    ]

    def set_s_mode(boolean):
        global single_player_survival
        single_player_survival = boolean

    survival_btns = [
        text_button((610, 275), 100, 50, "Single Player", set_s_mode, True, 35),
        text_button((610, 305), 100, 50, "Two Player (Default)", set_s_mode, False, 35),
    ]

    def set_mute_button(mute):
        global button_sound

        if mute:
            button_sound.set_volume(0)
        elif not mute:
            button_sound.set_volume(0.8)

    def set_mute(mute):
        global bullet_hit_sound, bullet_final_hit_sound, bullet_hit_asteroid, bullet_fire_sound, skeleton_sound, skeleton2_sound
        global loot_collect_sound, a_break_sound, a_hit_sound, granade_sound, ball_sound, title_clickked_a_lot_times_sound
        global normal_volume
        if mute:
            sounds = [
                bullet_hit_sound, bullet_final_hit_sound, bullet_hit_asteroid, bullet_fire_sound, skeleton_sound, skeleton2_sound,
                loot_collect_sound, a_break_sound, a_hit_sound, granade_sound, ball_sound, title_clickked_a_lot_times_sound
            ]
            for sound in sounds:
                sound.set_volume(0)
        elif not mute:
            a_break_sound.set_volume(1)
            granade_sound.set_volume(1)
            bullet_hit_asteroid.set_volume(1.5)
            bullet_final_hit_sound.set_volume(1.25)
            bullet_hit_sound.set_volume(normal_volume - 0.20)
            bullet_fire_sound.set_volume(normal_volume - 0.45)
            loot_collect_sound.set_volume(normal_volume - 0.05)
            a_hit_sound.set_volume(normal_volume)
            ball_sound.set_volume(normal_volume - 0.10)
            title_clickked_a_lot_times_sound.set_volume(normal_volume)
            skeleton2_sound.set_volume(1)
            skeleton_sound.set_volume(1)

    audio_btns = [
        text_button((210, 90), 100, 50, "On (Default)", set_mute, False, 35),
        text_button((210, 120), 100, 50, "Off", set_mute, True, 35),
    ]

    def set_coordinates(coords):
        global coordinates
        if coords:
            coordinates = True
        elif not coords:
            coordinates = False

    c_btns = [
        text_button((210, 90), 100, 50, "On", set_coordinates, True, 35),
        text_button((210, 120), 100, 50, "Off (Default)", set_coordinates, False, 35),
    ]

    def set_a_img(boolean):
        global a_img
        if boolean:
            a_img = True
        elif not boolean:
            a_img = False

    a_img_btns = [
        text_button((560, 90), 100, 50, "On (Default)", set_a_img, True, 35),
        text_button((560, 120), 100, 50, "Off", set_a_img, False, 35),
    ]

    def set_a_menu(boolean):
        global a_menu
        if boolean:
            a_menu = True
        elif not boolean:
            a_menu = False

    a_menu_btns = [
        text_button((560, 195), 100, 50, "On (Default)", set_a_menu, True, 35),
        text_button((560, 230), 100, 50, "Off", set_a_menu, False, 35),
    ]

    def set_classes(classes):
        global show_classes
        if classes:
            show_classes = True
        elif not classes:
            show_classes = False

    class_btns = [
        text_button((210, 195), 100, 50, "On (Default)", set_classes, True, 35),
        text_button((210, 230), 100, 50, "Off", set_classes, False, 35),
    ]

    def set_mute_bg(mute):
        global space_sound
        if mute:
            space_sound.set_volume(0)
        if not mute:
            space_sound.set_volume(0.60)

    bg_audio_btns = [
        text_button((210, 195), 100, 50, "On (Default)", set_mute_bg, False, 35),
        text_button((210, 230), 100, 50, "Off", set_mute_bg, True, 35),
    ]

    button_audio_btns = [
        text_button((210, 300), 100, 50, "On (Default)", set_mute_button, False, 35),
        text_button((210, 335), 100, 50, "Off", set_mute_button, True, 35),
    ]

    def set_credits_music_mute(mute):
        global credits_music
        if mute:
            credits_music.set_volume(0)
        if not mute:
            credits_music.set_volume(normal_volume)

    credits_audio_btns = [
        text_button((210, 400), 100, 50, "On (Default)", set_credits_music_mute, False, 35),
        text_button((210, 435), 100, 50, "Off", set_credits_music_mute, True, 35),
    ]

    def set_hitboxes(boxes):
         global red_spaceship, red_spaceship_image, yellow_spaceship, yellow_spaceship_image, asteroid_image
         global hitboxes

         if boxes:
             hitboxes = True
             asteroid_image = pygame.image.load(os.path.join('Assets', 'asteroid.png')).convert()
             red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90).convert()
             yellow_spaceship = pygame.transform.rotate(pygame.transform.scale(yellow_spaceship_image, (p_width, p_height)), -90).convert()
         elif not boxes:
             hitboxes = False
             asteroid_image = pygame.image.load(os.path.join('Assets', 'asteroid.png'))
             red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90)
             yellow_spaceship = pygame.transform.rotate(pygame.transform.scale(yellow_spaceship_image, (p_width, p_height)), -90)


    hitbox_btns = [
        text_button((210,300), 100, 50, "On", set_hitboxes, True, 35),
        text_button((210,335), 100, 50, "Off (Default)", set_hitboxes, False, 35)
    ]

    def set_timer(timer):
        global show_timer
        if timer:
            show_timer = True
        elif not timer:
            show_timer = False

    timer_btns = [
        text_button((210,405), 100, 50, "On (Default)", set_timer, True, 35),
        text_button((210,440), 100, 50, "Off", set_timer, False, 35)
    ]

    def set_a_counter(boolean):
        global a_counter
        if boolean:
            a_counter = True
        elif not boolean:
            a_counter = False

    a_counter_btns = [
        text_button((210,505), 100, 50, "On", set_a_counter, True, 35),
        text_button((210,540), 100, 50, "Off (Default)", set_a_counter, False, 35)
    ]

    def set_loot_spawn(spawn):
        global spawn_loots
        if spawn:
            spawn_loots = True
        elif not spawn:
            spawn_loots = False

    i_btns = [
        text_button((210,405), 100, 50, "On (Default)", set_loot_spawn, True, 35),
        text_button((210,440), 100, 50, "Off", set_loot_spawn, False, 35),
    ]

    def set_s_d(mode):
        global difficulty

        if mode == "Easy":
            difficulty = difficulty_list[0]
        elif mode == "Normal":
            difficulty = difficulty_list[1]
        elif mode == "Hard":
            difficulty = difficulty_list[2]
        elif mode == "Very Hard":
            difficulty = difficulty_list[3]

    s_difficulty_btns = [
        text_button((610,405), 100, 50, "Easy", set_s_d, "Easy", 35),
        text_button((610,440), 100, 50, "Normal (Default)", set_s_d, "Normal", 35),
        text_button((610,475), 100, 50, "Hard", set_s_d, "Hard", 35),
        text_button((610,510), 100, 50, "Very Hard", set_s_d, "Very Hard", 35)
    ]

    def set_ast_spawn(spawn):
        global spawn_asteroids_boolean

        if spawn:
            spawn_asteroids_boolean = True
        elif not spawn:
            spawn_asteroids_boolean = False

    ast_btns = [
        text_button((210,510), 100, 50, "On (Default)", set_ast_spawn, True, 35),
        text_button((210,545), 100, 50, "Off", set_ast_spawn, False, 35),
    ]

    x_txt = menu_fourty_font.render("X", True, white)
    custom_window_mode_btn = text_button((560, 605), 100, 50, "Use Custom Window Size", set_custom_mode, "", 35)
    display_btns.append(custom_window_mode_btn)

    clock = pygame.time.Clock()
    run = True
    while run:
        clock.tick(fps)
        if space_sound.get_num_channels() < 1:
            space_sound.play()
        for event in pygame.event.get():
            if appearance:
                for btn in p_btns:
                    btn.click(event)
                for btn in player1_btns:
                    btn.click(event)
                for btn in player2_btns:
                    btn.click(event)
                for btn in gamemodecolor_btns:
                    btn.click(event)
            if gameplay:
                for btn in a_btns:
                    btn.click(event)
                for btn in t_btns:
                    btn.click(event)
                for btn in i_btns:
                    btn.click(event)
                for btn in ast_btns:
                    btn.click(event)
                for btn in survival_btns:
                    btn.click(event)
                for btn in s_difficulty_btns:
                    btn.click(event)
            if display:
                for btn in display_btns:
                    btn.click(event)
                custom_w.handle_event(event)
                custom_h.handle_event(event)
            if audio:
                for btn in audio_btns:
                    btn.click(event)
                for btn in bg_audio_btns:
                    btn.click(event)
                for btn in button_audio_btns:
                    btn.click(event)
                for btn in credits_audio_btns:
                    btn.click(event)
            if other:
                for btn in c_btns:
                    btn.click(event)
                for btn in class_btns:
                    btn.click(event)
                for btn in hitbox_btns:
                    btn.click(event)
                for btn in timer_btns:
                    btn.click(event)
                for btn in a_counter_btns:
                    btn.click(event)
                for btn in a_img_btns:
                    btn.click(event)
                for btn in a_menu_btns:
                    btn.click(event)
            for button in buttons:
                button.click(event)
            if event.type == settings_exit:
                run = False
                menu()
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    run = False
                    menu()

        window.blit(bg_image, (0,0))

        pygame.draw.rect(window, (110,110,110, 100), pygame.Rect(0,0, 195, height))

        if gameplay:
            text = menu_normal_font.render("Gameplay settings", 1, white)
            subtitle = menu_fourty_font.render("Amount of wins needed to win tournament: ", 1, white)
            window.blit(text, (205, 5))
            window.blit(subtitle, (207, 55))
            subtitle2 = menu_fourty_font.render("Amount of health:", 1, white)
            window.blit(subtitle2, (207, 245))
            subtitle3 = menu_fourty_font.render("Spawn items: ", 1, white)
            window.blit(subtitle3, (207, 370))
            subtitle4 = menu_fourty_font.render("Spawn asteroids: ", 1, white)
            window.blit(subtitle4, (207, 475))
            subtitle5 = menu_fourty_font.render("Survival version:", 1, white)
            window.blit(subtitle5, (600, 245))
            subtitle3 = menu_fourty_font.render("Survival difficulty:", 1, white)
            window.blit(subtitle3, (600, 370))

            for button in t_btns:
                button.draw(window)
            for button in a_btns:
                button.draw(window)
            for button in i_btns:
                button.draw(window)
            for button in ast_btns:
                button.draw(window)
            for button in survival_btns:
                button.draw(window)
            for button in s_difficulty_btns:
                button.draw(window)


        elif display:
            text = menu_normal_font.render("Display settings", 1, white)
            window.blit(text, (205, 5))
            for button in display_btns:
                button.draw(window)

            custom_w.draw(window)
            custom_h.draw(window)
            window.blit(x_txt, (370, 605))
        elif appearance:
            text = menu_normal_font.render("Appearance settings", 1, white)
            subtitle = menu_fourty_font.render("Particles:", 1, white)
            window.blit(text, (205, 5))
            window.blit(subtitle, (207, 55))
            subtitle2 = menu_fourty_font.render("Player 1 Color:", 1, white)
            subtitle3 = menu_fourty_font.render("Player 2 Color:", 1, white)
            window.blit(subtitle2, (207, 155))
            window.blit(subtitle3, (207, 370))
            subtitle4 = menu_fourty_font.render("Selected gamemode accent color:", 1, white)
            window.blit(subtitle4, (500, 155))

            for btn in p_btns:
                btn.draw(window)
            for btn in player1_btns:
                btn.draw(window)
            for btn in player2_btns:
                btn.draw(window)
            for btn in gamemodecolor_btns:
                btn.draw(window)
        elif audio:
            text = menu_normal_font.render("Audio settings", 1, white)
            subtitle = menu_fourty_font.render("Sound Effects: ", 1, white)
            window.blit(text, (205, 5))
            window.blit(subtitle, (207, 55))
            subtitle2 = menu_fourty_font.render("Menu Background Sound: ", 1, white)
            window.blit(subtitle2, (207, 155))
            subtitle3 = menu_fourty_font.render("Button Click Sound: ", 1, white)
            window.blit(subtitle3, (207, 265))
            subtitle4 = menu_fourty_font.render("Credits Music", 1, white)
            window.blit(subtitle4, (207, 365))

            for btn in audio_btns:
                btn.draw(window)
            for btn in bg_audio_btns:
                btn.draw(window)
            for btn in button_audio_btns:
                btn.draw(window)
            for btn in credits_audio_btns:
                btn.draw(window)

        elif other:
            text = menu_normal_font.render("Other settings", 1, white)
            subtitle = menu_fourty_font.render("Show Coordinates: ", 1, white)
            window.blit(text, (205, 5))
            window.blit(subtitle, (207, 55))
            subtitle2 = menu_fourty_font.render("Show Classes: ", 1, white)
            window.blit(subtitle2, (207, 155))
            subtitle3 = menu_fourty_font.render("Show Hitboxes: ", 1, white)
            window.blit(subtitle3, (207, 265))
            subtitle4 = menu_fourty_font.render("Show Timer: ", 1, white)
            window.blit(subtitle4, (207, 370))
            subtitle5 = menu_fourty_font.render("Show Asteroid Counter: ", 1, white)
            window.blit(subtitle5, (207, 470))
            subtitle6 = menu_fourty_font.render("Render asteroid image: ", 1, white)
            window.blit(subtitle6, (550, 55))
            subtitle7 = menu_fourty_font.render("Spawn asteroids on menu:", 1, white)
            window.blit(subtitle7, (550, 155))

            for btn in c_btns:
                btn.draw(window)

            for btn in class_btns:
                btn.draw(window)

            for btn in hitbox_btns:
                btn.draw(window)

            for btn in timer_btns:
                btn.draw(window)

            for btn in a_counter_btns:
                btn.draw(window)

            for btn in a_img_btns:
                btn.draw(window)

            for btn in a_menu_btns:
                btn.draw(window)

        for button in buttons:
            button.draw(window)

        pygame.display.update()

def draw_credits(texts, texts_y):
     window.blit(bg_image, (0, 0))

     for text in texts:
         window.blit(text, (width/2 - text.get_width()/2, height + int(texts_y[texts.index(text)])))

     pygame.display.update()

def credits():
    clock = pygame.time.Clock()
    run = True

    texts = [
        menu_title_smaller_font.render("Space Game", 1, white),
        menu_normal_font.render("By Oras Pönkä in 2022-2023", 1, white),
        menu_medium_font.render("Programming, UI Designing, Gitlab Page and Playtesting", 1, white),
        menu_medium_font.render("Oras Pönkä", 1, white),
        menu_medium_font.render("2D Art", 1, white),
        menu_medium_font.render("Skorpio (opengameart)", 1, white),
        menu_medium_font.render("wubitog (opengameart)", 1, white),
        menu_medium_font.render("pixelartmaker.com (Creator id : 9664c4)", 1, white),
        menu_medium_font.render("kirocookie (opengameart)", 1, white),
        menu_medium_font.render("Ryan Cunningham (CuteWallpaper.org)", 1, white),
        menu_medium_font.render("Background Image", 1, white),
        menu_medium_font.render("Kai Pilger", 1, white),
        menu_medium_font.render("Logo Font", 1, white),
        menu_medium_font.render("Boba Fonts (dafont.com)", 1, white),
        menu_medium_font.render("Sound Effects", 1, white),
        menu_medium_font.render("Jesús Lastra (opengameart)", 1, white),
        menu_medium_font.render("Lucas Calvo (opengameart/mundosound.com)", 1, white),
        menu_medium_font.render("freesound.org", 1, white),
        menu_medium_font.render("mixkit.co", 1, white),
        menu_medium_font.render("Music", 1, white),
        menu_medium_font.render("Alexandr Zhelanov (opengameart/soundcloud)", 1, white),
        menu_medium_font.render("Special Thanks", 1, white),
        menu_medium_font.render("Tech With Tim (Youtube)", 1, white),
        menu_medium_font.render("ScriptLine Studios (Youtube)", 1, white),
        menu_medium_font.render("Aatos Pönkä", 1, white),
        menu_medium_font.render("Pirta Pönkä", 1, white),
        menu_medium_font.render("Thank you a lot for playing.", 1, white),



    ]

    texts_y = [
        50,
        170,
        270,
        300,
        360,
        390,
        420,
        450,
        480,
        510,
        570,
        600,
        660,
        690,
        750,
        780,
        810,
        840,
        870,
        930,
        960,
        1010,
        1040,
        1070,
        1100,
        1130,
        1190,

    ]


    credits_music.play()

    while run and texts_y[-1] + height > -100:
        clock.tick(35)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    run = False

        for text_y in texts_y:
            texts_y[texts_y.index(text_y)] = int(text_y - 1)

        draw_credits(texts, texts_y)
    credits_music.stop()
    menu()

        
title_clickked = 0
version_clickked = 0

def set_potato(asteroids):
    global asteroid_image, title_clickked, potato
    title_clickked += 1
    if title_clickked == 15:
        potato = True
        title_clickked_a_lot_times_sound.play()
        asteroid_image = pygame.image.load(os.path.join('Assets', 'potato.png'))
        for a in asteroids:
           a.asteroid = pygame.transform.scale(asteroid_image, (a.w, a.h))
    if title_clickked == 30:
        skeleton2_sound.play()
        asteroid_image = pygame.image.load(os.path.join('Assets', 'spaghetti.png'))
        for a in asteroids:
           a.asteroid = pygame.transform.scale(asteroid_image, (a.w, a.h))

def quit_func(nothing):
    pygame.event.post(pygame.event.Event(quit_e))


def menu_func(nothing):
    pygame.event.post(pygame.event.Event(backmenu_e))

def draw_quit_menu(quit_btn, backmenu_btn):
    window.blit(bg_image, (0,0))
    quit_btn.draw(window)
    backmenu_btn.draw(window)
    pygame.display.update()

def quit_menu():
    run = True
    clock = pygame.time.Clock()


    quit_txt = pygame.font.SysFont(font, 45).render("Quit (1)", 1, white)
    backmenu_txt = pygame.font.SysFont(font, 45).render("Bakc to menu (2)", 1, white)

    quit_btn = text_button((width / 2 - quit_txt.get_width() / 2, height / 2), 50, 50, "Quit (1)", quit_func, "", 45)
    backmenu_btn = text_button((width / 2 - backmenu_txt.get_width() / 2, height / 2 - 35), 50, 50, "Back to menu (2)", menu_func, "", 45)

    while run:
        clock.tick(fps)
        for event in pygame.event.get():
            quit_btn.click(event)
            backmenu_btn.click(event)
            if event.type == pygame.QUIT or event.type == quit_e:
                run = False
                pygame.quit()
            if event.type == backmenu_e:
                run = False
                menu()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1 or event.key == pygame.K_KP_1:
                    run = False
                    pygame.quit()
                if event.key == pygame.K_2 or event.key == pygame.K_KP_2:
                    run = False
                    menu()

        draw_quit_menu(quit_btn, backmenu_btn)

def menu():
    asteroids = [
    ]

    if a_menu:
        for i in range(randrange(1,10)):
            asteroids.append(asteroid_rect(randrange(0, width), randint(0, height), True, False))

    index = 1
    turnaument = False
    ballmode = False
    race = False
    if gamemode == 1:
        turnaument = False
        ballmode = False
        race = False
        index = 1
    elif gamemode == 2:
        turnaument = True
        ballmode = False
        race = False
        index = 2
    elif gamemode == 3:
        turnaument = False
        ballmode = True
        race = False
        index = 3
    elif gamemode == 4:
        turnaument = False
        ballmode = False
        race = True
        index = 0

    clock = pygame.time.Clock()
    run = True
    particles = []

    title = menu_title_font.render("Space Game", 1, white)
    title_rect = pygame.Rect(width/2 - title.get_width()/2, height/2 - title.get_height()/2, title.get_width(), title.get_height())

    version_text = menu_small_font.render("v1.11.5", 1, white)
    version_txt_rect = pygame.Rect(width - version_text.get_width() - 5, height - version_text.get_height() - 5, version_text.get_width(), version_text.get_height())

    while run:
        clock.tick(fps)
        if space_sound.get_num_channels() < 1:
            space_sound.play()
        for event in pygame.event.get():
            buttoni.click(event)
            buttoni2.click(event)
            buttoni3.click(event)
            buttoni4.click(event)
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    if version_txt_rect.collidepoint(pygame.mouse.get_pos()):
                        global version_clickked
                        global gamemode_color
                        version_clickked += 1
                        if version_clickked == 3:
                            gamemode_color = (0, 255, 255)
                            skeleton_sound.play()
                    if title_rect.collidepoint(pygame.mouse.get_pos()):
                        set_potato(asteroids)
                    for a in asteroids:
                        if a.rect.collidepoint(pygame.mouse.get_pos()):
                            spawn_particles_asteroid(particles, a.rect.x + a.rect.width / 2, a.rect.y + a.rect.height / 2)
                            a_break_sound.play()
                            a.rect.x = 0 - a.rect.width
            if event.type == open_settings:
                run = False
                settings()
            if event.type == open_tutorial:
                run = False
                tutorial()
            if event.type == open_classes:
                run = False
                class_menu()
            if event.type == open_credits:
                run = False
                space_sound.stop()
                credits()
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
            if event.type == pygame.MOUSEWHEEL and event:
                if event.y == -1:
                    index -= 1
                    if index < 1:
                        index = 4
                if event.y == 1:
                    index += 1
                    if index > 4:
                        index = 1

                if index == 1:
                    turnaument = False
                    ballmode = False
                    race = False
                elif index == 2:
                    turnaument = True
                    ballmdoe = False
                    race = False
                elif index == 3:
                    turnaument = False
                    ballmode = True
                    race = False
                elif index == 4:
                    turnaument = False
                    ballmode = False
                    race = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1 or event.key == pygame.K_KP_1:
                    turnaument = False
                    ballmode = False
                    race = False
                if event.key == pygame.K_2 or event.key == pygame.K_KP_2:
                    turnaument = True
                    ballmode = False
                    race = False
                if event.key == pygame.K_3 or event.key == pygame.K_KP_3:
                    turnaument = False
                    ballmode = True
                    race = False
                if event.key == pygame.K_4 or event.key == pygame.K_KP_4:
                    turnaument = False
                    ballmode = False
                    race = True
                if event.key == pygame.K_d or event.key == pygame.K_RIGHT:
                    index += 1
                    if index > 4:
                        index = 1
                    if index == 1:
                        turnaument = False
                        ballmode = False
                        race = False
                    elif index == 2:
                        turnaument = True
                        ballmdoe = False
                        race = False
                    elif index == 3:
                        turnaument = False
                        ballmode = True
                        race = False
                    elif index == 4:
                        turnaument = False
                        ballmode = False
                        race = True
                if event.key == pygame.K_a or event.key == pygame.K_LEFT:
                    index -= 1
                    if index < 1:
                        index = 4
                    if index == 1:
                        turnaument = False
                        ballmode = False
                        race = False
                    elif index == 2:
                        turnaument = True
                        ballmdoe = False
                        race = False
                    elif index == 3:
                        turnaument = False
                        ballmode = True
                        race = False
                    elif index == 4:
                        turnaument = False
                        ballmode = False
                        race = True
                if event.key == pygame.K_s:
                    run = False
                    settings()
                if event.key == pygame.K_c:
                    run = False
                    class_menu()
                if event.key == pygame.K_TAB:
                    run = False
                    tutorial()
                if event.key == pygame.K_ESCAPE:
                    run = False
                    quit_menu()
                if event.key == pygame.K_SPACE or event.key == pygame.K_RETURN:
                    run = False
                    space_sound.stop()
                    game(turnaument, 0, 0, ballmode, race)
        if run:
            handle_asteroids_menu(asteroids)
            draw_menu(asteroids, turnaument, particles, ballmode, race)

def disable_shield(yellow_shield, red_shield):
    def disable():
         if yellow_shield:
             pygame.event.post(pygame.event.Event(yellow_sd))
         if red_shield:
             pygame.event.post(pygame.event.Event(red_sd))
    shieldTimer = Timer(5, disable)
    shieldTimer.start()

def disable_dd(yellow_dd, red_dd):
    def disable():
         if yellow_dd:
             pygame.event.post(pygame.event.Event(yellow_d_dd))
         if red_dd:
             pygame.event.post(pygame.event.Event(red_d_dd))
    doubledamageTimer = Timer(5, disable)
    doubledamageTimer.start()

def spawn_asteroids(asteroids, a_send):
    def spawn():
        if len(asteroids) == 0:
            if randint(1,16) == randrange(1,16):
                for i in range(randrange(7,15)):
                    pygame.event.post(pygame.event.Event(asteroid_false))
                    if choice([True, False]):
                        pygame.event.post(pygame.event.Event(a_spawned))
                        asteroids.append(asteroid_rect(width, randrange(10, height - 25), True, False))
                    else:
                        pygame.event.post(pygame.event.Event(a_spawned))
                        asteroids.append(asteroid_rect(0, randrange(10, height - 25), False, False))
            else:
                for i in range(choice([1,1,2,2,3,3,3,4,4,5,6])):
                    pygame.event.post(pygame.event.Event(asteroid_false))
                    if choice([True, False]):
                        pygame.event.post(pygame.event.Event(a_spawned))
                        asteroids.append(asteroid_rect(width, randrange(10, height - 25), True, False))
                    else:
                        pygame.event.post(pygame.event.Event(a_spawned))
                        asteroids.append(asteroid_rect(0, randrange(10, height - 25), False, False))

    t = Timer(randrange(1, 16), spawn)
    t.start()


def spawn_asteroids_race(asteroids, a_send):
    def spawn():
        for i in range(randrange(randrange(difficulty[0],difficulty[1]), randrange(difficulty[2],difficulty[3]))):
            pygame.event.post(pygame.event.Event(a_spawned))
            asteroids.append(asteroid_rect(randrange(width, width + 100), randrange(10, height - 25), True, True))
        pygame.event.post(pygame.event.Event(asteroid_false))

    t = Timer(1, spawn)
    t.start()

def check_a_pos(asteroids):
    returnValue = True

    for a in asteroids:
        if a.rect.x > width/2 + width / 3.5:
            returnValue = False

    return returnValue


def handle_ball(ball, red_bullets, yellow_bullets, red, yellow):
    global ball_y_speed, ball_x_speed

    ball_center = ball.y + ball.height / 2

    for bullet in red_bullets:
        if ball.colliderect(bullet):
            hit_point = bullet.y - ball_center
            if hit_point < -8:
                ball_y_speed += ball_speed
                ball_x_speed += -ball_speed
            elif hit_point > 8:
                ball_y_speed += -ball_speed
                ball_x_speed += -ball_speed
            else:
                ball_x_speed += -ball_speed
            red_bullets.remove(bullet)
            ball_sound.play()

    for bullet in yellow_bullets:
        if ball.colliderect(bullet):
            hit_point = bullet.y - ball_center
            if hit_point < -8:
                ball_y_speed += ball_speed
                ball_x_speed += ball_speed
            elif hit_point > 8:
                ball_y_speed += -ball_speed
                ball_x_speed += ball_speed
            else:
                ball_x_speed += ball_speed
            yellow_bullets.remove(bullet)
            ball_sound.play()

    if ball.colliderect(red):
        hit_point = (red.y - red.height / 2) - ball_center
        if  -85 <= hit_point <= -70:
            ball_y_speed += ball_speed
            ball_x_speed -= ball_speed
        elif -41 <= hit_point <= -23:
            ball_y_speed += -ball_speed
            ball_x_speed -= ball_speed
        else:
            ball_x_speed -= ball_speed
        ball_sound.play()
    if ball.colliderect(yellow):
        hit_point = (yellow.y - yellow.height / 2) - ball_center
        if  -85 <= hit_point <= -70:
            ball_y_speed += ball_speed
            ball_x_speed += ball_speed
        elif -41 <= hit_point <= -23:
            ball_y_speed += -ball_speed
            ball_x_speed += ball_speed
        else:
            ball_x_speed += ball_speed
        ball_sound.play()

    if ball_y_speed < -ball_max:
        ball_y_speed = -ball_max
    if ball_y_speed > ball_max:
        ball_y_speed = ball_max

    if ball_x_speed < -ball_max:
        ball_x_speed = -ball_max
    if ball_x_speed > ball_max:
        ball_x_speed = ball_max

    if ball.y + ball_y_speed > 20 and ball.y + ball_y_speed + ba_height < height - 20:
        ball.y += ball_y_speed

    ball.x += ball_x_speed

    if ball_y_speed < 0 and ball_y_speed + 1 < 1:
        ball_y_speed += 1
    if ball_y_speed > 0 and ball_y_speed - 1 > -1:
        ball_y_speed -= 1
    if ball_x_speed < 0 and ball_x_speed + 1 < 1:
        ball_x_speed += 1
    if ball_x_speed > 0 and ball_x_speed - 1 > -1:
        ball_x_speed -= 1


def handle_granades(yellow_granades, red_granades, yellow, red, particles, loots, lootTypes):
    for g in yellow_granades:
        removed = False
        if g.rect.colliderect(red):
            pygame.event.post(pygame.event.Event(red_hit_g))
            spawn_particles(particles, g.rect.x + g.rect.width / 2, g.rect.y + g.rect.height / 2)
            yellow_granades.remove(g)
            removed = True

        if removed == False:
            for loot in loots:
                if loot.colliderect(g.rect):
                    spawn_particles(particles, g.rect.x + g.rect.width / 2, g.rect.y + g.rect.height / 2)
                    granade_sound.play()
                    lootTypes.remove(lootTypes[loots.index(loot)])
                    loots.remove(loot)
                    pygame.event.post(pygame.event.Event(loot_get))
                    yellow_granades.remove(g)
                    removed = True

    for g in red_granades:
        removed = False
        if g.rect.colliderect(yellow):
            pygame.event.post(pygame.event.Event(yellow_hit_g))
            spawn_particles(particles, g.rect.x + g.rect.width / 2, g.rect.y + g.rect.height / 2)
            red_granades.remove(g)
            removed = True

        if removed == False:
            for loot in loots:
                if loot.colliderect(g.rect):
                    spawn_particles(particles, g.rect.x + g.rect.width / 2, g.rect.y + g.rect.height / 2)
                    granade_sound.play()
                    lootTypes.remove(lootTypes[loots.index(loot)])
                    loots.remove(loot)
                    pygame.event.post(pygame.event.Event(loot_get))
                    red_granades.remove(g)
                    removed = True

class regeneration():
    def __init__(self):
        self.t = Timer(10, self.regenerate)
    def regenerate(self):
        pygame.event.post(pygame.event.Event(reg_send))
    def start(self):
        self.t = Timer(10, self.regenerate)
        self.t.start()

class challenger_effect():
    def __init__(self):
        self.t = Timer(12, self.effect)
    def effect(self):
        pygame.event.post(pygame.event.Event(challenger_send))
    def start(self):
        self.t = Timer(12, self.effect)
        self.t.start()

def game(turnaument, red_wins, yellow_wins, ballmode, race):
    global coordinates
    global red_class
    global yellow_class
    global bullets
    global max_granades
    global ball_y_speed, ball_x_speed
    global red_spaceship
    global border
    global gamemode
    global a_counter

    if not turnaument and not race and not ballmode:
        gamemode = 1
    if turnaument:
        gamemode = 2
    if ballmode:
        gamemode = 3
    if race:
        gamemode = 4

    if not race:
        border = pygame.Rect(width/2 - 2.5, 0, 5, height)
    elif race:
        border = pygame.Rect(width/2.5 - 2.5, 0, 5, height)

    if not race:
        red = pygame.Rect(width - 120, height/2 - p_height / 2, p_height, p_width)
        yellow = pygame.Rect(120 - p_width, height/2 - p_height / 2, p_height, p_width)
        if not hitboxes:
            red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90)
        elif hitboxes:
            red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), 90).convert()
    elif race and not single_player_survival:
        if not hitboxes:
            red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), -90)
        elif hitboxes:
            red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), -90).convert()
        red = pygame.Rect(120 - p_width, height - 100, p_height, p_width)
        yellow = pygame.Rect(120 - p_width, 100, p_height, p_width)
    elif race and single_player_survival:
        if not hitboxes:
            red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), -90)
        elif hitboxes:
            red_spaceship = pygame.transform.rotate(pygame.transform.scale(red_spaceship_image, (p_width, p_height)), -90).convert()
        red = pygame.Rect(100000 - p_width, height - 100000, p_height, p_width)
        yellow = pygame.Rect(120 - p_width, height / 2 - p_height / 2, p_height, p_width)


    ball = pygame.Rect(width / 2 - ba_width / 2, height/2 - ba_height / 2, ba_width, ba_height)

    red_bullets = []
    yellow_bullets = []

    ball_x_speed = 0
    ball_y_speed = 0

    asteroids_destroyed = 0
    asteroids_spawned = 0

    asteroids = []
    a_send = False
    loots = []
    lootTypes = []
    send = True

    particles = []

    loot_texts = []

    red_wins = red_wins
    yellow_wins = yellow_wins

    yellow_granades = []
    red_granades = []

    yellow_mspeed = 0
    red_mspeed = 0
    yellow_bspeed = 0
    red_bspeed = 0
    yellow_shield = False
    red_shield = False

    if not race:
        yellow_dd = False
        red_dd = False
    elif race:
        yellow_dd = True
        red_dd = True

    red_c_bspeed = 0
    red_c_mspeed = 0
    red_c_mines = 0
    red_c_bullets = 0
    red_c_ddc = False

    yellow_c_bspeed = 0
    yellow_c_mspeed = 0
    yellow_c_mines = 0
    yellow_c_bullets = 0
    yellow_c_ddc = False

    yellow_immunity = False
    red_immunity = False

    quit = False

    winner_text = ""
    reason = ""

    yellow_regeneration = False
    red_regeneration = False
    yellow_challenger = False
    red_challenger = False

    yellow_vampire = False
    red_vampire = False

    red_v_hits = 0
    yellow_v_hits = 0

    if not race:
        red_hp = default_hp
        yellow_hp = default_hp
    elif race:
        red_hp = 1
        yellow_hp = 1

    if ballmode or race:
        red_class = "None"
        yellow_class = "None"

    if ballmode:
        max_granades = 0
        bullets = 5
    elif race:
        max_granades = 0
        bullets = 5
    else:
        max_granades = 3
        bullets = 5

    if red_class == "Tank":
        red_hp += 6
        red_c_mspeed = -3
        red_c_bspeed = 1
        red_c_mines = 1
        red_c_bullets = -1
    elif red_class == "Engineer":
        red_c_mines = 3
        red_c_bullets = -2
        red_c_bspeed = -1
        red_c_mspeed = 1
        red_hp -= 2
    elif red_class == "Sniper":
        red_c_bullets = -1
        red_c_mines = -1
        red_hp -= 1
        red_c_bspeed = 3
    elif red_class == "Lucky":
        red_c_ddc = True
        red_c_mines = -1
        red_c_mspeed = 1
        red_c_bullets = -2
        red_c_bspeed = 1
    elif red_class == "Assassin":
        red_c_mspeed = 3
        red_c_mines = -1
        red_c_bullets = -1
        red_hp -= 2
    elif red_class == "Mad":
        red_hp -= 4
        red_c_mspeed = 2
        red_c_bspeed = 2
        red_c_mines = 1
        red_c_bullets = -1
    elif red_class == "Healer":
        red_hp -= 3
        red_c_mspeed = 2
        red_regeneration = True
        red_c_bullets = -1
        red_c_mines = -1
    elif red_class == "Challenger":
        red_hp -= 2
        red_challenger = True
        red_c_bspeed = -1
        red_c_mspeed = -1
        red_c_mines = -1
    elif red_class == "Scout":
        red_hp -= 3
        red_c_bspeed = 1
        red_c_mspeed = 1
        red_c_mines = -3
        red_c_bullets = 1
        red_immunity = True
    elif red_class == "Vampire":
        red_hp -= 3
        red_c_bullets = -1
        red_c_mines = -1
        red_c_mspeed = 1
        red_c_bspeed = 1
        red_vampire = True

    if yellow_class == "Tank":
        yellow_hp += 6
        yellw_c_bspeed = 1
        yellow_c_mspeed = -3
        yellow_c_mines = 1
        yellow_c_bullets = -1
    elif yellow_class == "Engineer":
        yellow_c_mines = 3
        yellow_c_bullets = -2
        yellow_c_bspeed = -1
        yellow_c_mspeed = 1
        yellow_hp -= 2
    elif yellow_class == "Sniper":
        yellow_hp -= 1
        yellow_c_bullets = -1
        yellow_c_mines = -1
        yellow_c_bspeed = 3
    elif yellow_class == "Lucky":
        yellow_c_ddc = True
        yellow_c_mines = -1
        yellow_c_mspeed = 1
        yellow_c_bullets = -2
        yellow_c_bspeed = 1
    elif yellow_class == "Assassin":
        yellow_c_mspeed = 3
        yellow_c_mines = -1
        yellow_c_bullets = -1
        yellow_hp -= 2
    elif yellow_class == "Mad":
        yellow_hp -= 4
        yellow_c_mspeed = 2
        yellow_c_bspeed = 2
        yellow_c_mines = 1
        yellow_c_bullets = -1
    elif yellow_class == "Healer":
        yellow_hp -= 3
        yellow_c_mspeed = 2
        yellow_c_bullets = -1
        yellow_c_mines = -1
        yellow_regeneration = True
    elif yellow_class == "Challenger":
        yellow_hp -= 2
        yellow_challenger = True
        yellow_c_bspeed = -1
        yellow_c_mspeed = -1
        yellow_c_mines = -1
    elif yellow_class == "Scout":
        yellow_hp -= 3
        yellow_c_bspeed = 1
        yellow_c_mspeed = 1
        yellow_c_mines = -3
        yellow_c_bullets = 1
        yellow_immunity = True
    elif yellow_class == "Vampire":
        yellow_hp -= 3
        yellow_c_bullets = -1
        yellow_c_mines = -1
        yellow_c_mspeed = 1
        yellow_c_bspeed = 1
        yellow_vampire = True


    regeneration_send = False
    c_effect_send = False

    reg_hp = regeneration()
    c_effect = challenger_effect()

    clock = pygame.time.Clock()
    start_time = pygame.time.get_ticks()
    run = True
    while run:
        clock.tick(fps)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_g:
                    if len(yellow_granades) < max_granades + yellow_c_mines:
                        yellow_granades.append(granade(yellow.x + yellow.width, yellow.y + yellow.height / 3, width // 55.88, yellow_g_speed, player1_bullet_color, False))
                if event.key == pygame.K_RSHIFT:
                    if len(red_granades) < max_granades + red_c_mines:
                        red_granades.append(granade(red.x, red.y + red.height / 3, width // 55.88, red_g_speed, player2_bullet_color, True))
                if event.key == pygame.K_KP_1:
                    if len(red_granades) < max_granades + red_c_mines:
                        red_granades.append(granade(red.x, red.y + red.height / 3, width // 55.88, red_g_speed, player2_bullet_color, True))
                if event.key == pygame.K_ESCAPE:
                    run = False
                    quit = True
                    if reg_hp.t.is_alive():
                        reg_hp.t.cancel()
                    if c_effect.t.is_alive():
                        c_effect.t.cancel()
                if event.key == pygame.K_f and len(yellow_bullets) < bullets + yellow_c_bullets:
                    bullet = pygame.Rect(yellow.x + yellow.width, yellow.y + yellow.height // 2, b_width, b_height)
                    yellow_bullets.append(bullet)
                    bullet_fire_sound.play()

                if event.key == pygame.K_RCTRL and len(red_bullets) < bullets + red_c_bullets:
                    if not race:
                        bullet = pygame.Rect(red.x, red.y + red.height // 2, b_width, b_height)
                        red_bullets.append(bullet)
                        bullet_fire_sound.play()
                    elif race and not single_player_survival:
                        bullet = pygame.Rect(red.x + red.width, red.y + red.height // 2, b_width, b_height)
                        red_bullets.append(bullet)
                        bullet_fire_sound.play()
                if event.key == pygame.K_KP_0 and len(red_bullets) < bullets + red_c_bullets:
                    if not race:
                        bullet = pygame.Rect(red.x, red.y + red.height // 2, b_width, b_height)
                        red_bullets.append(bullet)
                        bullet_fire_sound.play()
                    elif race and not single_player_survival:
                        bullet = pygame.Rect(red.x + red.width, red.y + red.height // 2, b_width, b_height)
                        red_bullets.append(bullet)
                        bullet_fire_sound.play()
                if event.key == pygame.K_END and len(red_bullets) < bullets + red_c_bullets:
                    if not race:
                        bullet = pygame.Rect(red.x, red.y + red.height // 2, b_width, b_height)
                        red_bullets.append(bullet)
                        bullet_fire_sound.play()
                    elif race and not single_player_survival:
                        bullet = pygame.Rect(red.x + red.width, red.y + red.height // 2, b_width, b_height)
                        red_bullets.append(bullet)
                        bullet_fire_sound.play()

                if event.key == pygame.K_F3:
                    if coordinates:
                        coordinates = False
                    else:
                        coordinates = True

                if event.key == pygame.K_F2:
                    if a_counter:
                        a_counter = False
                    else:
                        a_counter = True

            if event.type == yellow_hit:
                if red_vampire:
                    red_v_hits += 1
                if yellow_shield == False and red_dd == False:
                    if red_c_ddc:
                        if randrange(1,2) == randint(1,2):
                            if yellow_hp - 2 < 0:
                                yellow_hp = 0
                            else:
                                yellow_hp -= 2
                        else:
                            yellow_hp -= 1
                    else:
                        yellow_hp -= 1
                elif yellow_shield == False and red_dd == True:
                    if yellow_hp - 2 < 0:
                        yellow_hp = 0
                    else:
                        yellow_hp -= 2

                if yellow_hp == 0:
                    reason = "Player 1 killed by Player 2's bullet"
                    bullet_final_hit_sound.play()
                else:
                    bullet_hit_sound.play()
            if event.type == red_hit:
                if yellow_vampire:
                    yellow_v_hits += 1
                if red_shield == False and yellow_dd == False:
                    if yellow_c_ddc:
                        if randrange(1,2) == randint(1,2):
                            if red_hp - 2 < 0:
                                red_hp = 0
                            else:
                                red_hp -= 2
                        else:
                            red_hp -= 1
                    else:
                        red_hp -= 1
                elif red_shield == False and yellow_dd == True:
                    if red_hp - 2 < 0:
                        red_hp = 0
                    else:
                        red_hp -= 2

                if red_hp == 0:
                    reason = "Player 2 killed by Player 1's bullet"
                    bullet_final_hit_sound.play()
                else:
                    bullet_hit_sound.play()
            if event.type == loot_send:
                send = False
            if event.type == loot_get:
                send = True
            if event.type == yellow_hp_loot:
                yellow_hp += 3
                loot_collect_sound.play()
            if event.type == red_hp_loot:
                red_hp += 3
                loot_collect_sound.play()
            if event.type == red_m_loot:
                red_mspeed += 2
                loot_collect_sound.play()
            if event.type == red_b_loot:
                red_bspeed += 2
                loot_collect_sound.play()
            if event.type == yellow_m_loot:
                yellow_mspeed += 2
                loot_collect_sound.play()
            if event.type == yellow_b_loot:
                yellow_bspeed += 2
                loot_collect_sound.play()
            if event.type == yellow_s_loot:
                yellow_shield = True
                disable_shield(True, False)
                loot_collect_sound.play()
            if event.type == red_s_loot:
                red_shield = True
                disable_shield(False, True)
                loot_collect_sound.play()
            if event.type == red_sd:
                red_shield = False
            if event.type == yellow_sd:
                yellow_shield = False
            if event.type == yellow_hit_asteroid:
                if yellow_hp - 3 < 1 and yellow_shield == False and yellow_immunity == False:
                    reason = "Player 1 killed by asteroid"
                    bullet_final_hit_sound.play()
                    yellow_hp = 0
                else:
                    if yellow_shield == False and yellow_immunity == False:
                        a_hit_sound.play()
                        yellow_hp -= 3
                    else:
                        a_hit_sound.play()
            if event.type == red_hit_asteroid:
                if red_hp - 3 < 1 and red_shield == False and red_immunity == False:
                    reason = "Player 2 killed by asteroid"
                    bullet_final_hit_sound.play()
                    red_hp = 0
                else:
                    if red_shield == False and red_immunity == False:
                        a_hit_sound.play()
                        red_hp -= 3
                    else:
                        a_hit_sound.play()
            if event.type == asteroid_false:
                a_send = False
            if event.type == red_dd_loot:
                loot_collect_sound.play()
                red_dd = True
                disable_dd(False, True)
            if event.type == yellow_dd_loot:
                loot_collect_sound.play()
                yellow_dd = True
                disable_dd(True, False)
            if event.type == red_d_dd:
                red_dd = False
            if event.type == yellow_d_dd:
                yellow_dd = False
            if event.type == yellow_hit_g:
                if yellow_hp - 3 < 1 and yellow_shield == False:
                    reason = "Player 1 killed by Player 2's mine"
                    bullet_final_hit_sound.play()
                    yellow_hp = 0
                elif yellow_shield == False:
                    yellow_hp -= 2
                    granade_sound.play()
                elif yellow_shield == True:
                    granade_sound.play()
            if event.type == red_hit_g:
                if red_hp - 2 < 1 and red_shield == False:
                    reason = "Player 2 killed by Player 1's mine"
                    bullet_final_hit_sound.play()
                    red_hp = 0
                elif red_shield == False:
                    red_hp -= 2
                    granade_sound.play()
                elif red_shield == True:
                    granade_sound.play()
            if event.type == reg_send:
                regeneration_send = False
                if yellow_regeneration:
                    yellow_hp += 1
                if red_regeneration:
                    red_hp += 1
            if event.type == challenger_send:
                c_effect_send = False
                if yellow_challenger:
                    yellow_c_mspeed += 1
                    yellow_c_bspeed += 1
                if red_challenger:
                    red_c_mspeed += 1
                    red_c_bspeed += 1
            if event.type == yellow_d_asteroid:
                asteroids_destroyed += 1
            if event.type == a_spawned:
                asteroids_spawned += 1
            if event.type == yellow_joker_loot:
                yellow_hp += 1
                yellow_bspeed += 1
                yellow_mspeed += 1
            if event.type == red_joker_loot:
                red_hp += 1
                red_bspeed += 1
                red_mspeed += 1

        if run:
            if ballmode:
                if ball.x < 0:
                    winner_text = "Player 2 Wins"
                if ball.x + ba_width > width:
                    winner_text = "Player 1 Wins"
                if winner_text != "":
                    particles = []
                    draw_window(red, yellow, red_bullets, yellow_bullets, red_hp, yellow_hp, coordinates, loots, lootTypes, yellow_shield, red_shield, particles, loot_texts, asteroids, red_dd, yellow_dd, yellow_granades, red_granades, pygame.time.get_ticks() - start_time, turnaument, red_wins, yellow_wins, yellow_c_bullets, red_c_bullets, yellow_c_mines, red_c_mines, ballmode, ball, race, asteroids_destroyed, asteroids_spawned)
                    draw_winner(winner_text, "", "")
                    break
            elif race and single_player_survival:
                if yellow_hp <= 0:
                    winner_text = "Defeat"
                    particles = []
                    draw_window(red, yellow, red_bullets, yellow_bullets, red_hp, yellow_hp, coordinates, loots, lootTypes, yellow_shield, red_shield, particles, loot_texts, asteroids, red_dd, yellow_dd, yellow_granades, red_granades, pygame.time.get_ticks() - start_time, turnaument, red_wins, yellow_wins, yellow_c_bullets, red_c_bullets, yellow_c_mines, red_c_mines, ballmode, ball, race, asteroids_destroyed, asteroids_spawned)
                    time = pygame.time.get_ticks() - start_time
                    minutes = str(int(time/60000)).zfill(2)
                    seconds = str( int((time%60000)/1000) ).zfill(2)
                    draw_winner(winner_text, "Your time was {} minutes and {} seconds.".format(minutes, seconds), "You destroyed {} asteroids".format(asteroids_destroyed))
                    break
            elif not turnaument and not ballmode:
                if red_hp <= 0:
                    winner_text = "Player 1 Wins!"
                if yellow_hp <= 0:
                    winner_text = "Player 2 Wins!"
                if winner_text != "":
                    particles = []
                    draw_window(red, yellow, red_bullets, yellow_bullets, red_hp, yellow_hp, coordinates, loots, lootTypes, yellow_shield, red_shield, particles, loot_texts, asteroids, red_dd, yellow_dd, yellow_granades, red_granades, pygame.time.get_ticks() - start_time, turnaument, red_wins, yellow_wins, yellow_c_bullets, red_c_bullets, yellow_c_mines, red_c_mines, ballmode, ball, race, asteroids_destroyed, asteroids_spawned)
                    draw_winner(winner_text, reason, "")
                    break
            elif turnaument:
                if red_hp <= 0 and yellow_wins + 1 < tournament_wins:
                    winner_text = "Player 1 Wins this match!"
                    yellow_wins += 1
                elif red_hp <= 0 and yellow_wins + 1 == tournament_wins:
                    yellow_wins += 1
                    winner_text = "Player 1 Wins this tournament!"
                if yellow_hp <= 0 and red_wins + 1 < tournament_wins:
                    winner_text = "Player 2 Wins this match!"
                    red_wins += 1
                elif yellow_hp <= 0 and red_wins + 1 == tournament_wins:
                    red_wins += 1
                    winner_text = "Player 2 Wins this tournament!"
                if winner_text != "":
                    particles = []
                    draw_window(red, yellow, red_bullets, yellow_bullets, red_hp, yellow_hp, coordinates, loots, lootTypes, yellow_shield, red_shield, particles, loot_texts, asteroids, red_dd, yellow_dd, yellow_granades, red_granades, pygame.time.get_ticks() - start_time, turnaument, red_wins, yellow_wins, yellow_c_bullets, red_c_bullets, yellow_c_mines, red_c_mines, ballmode, ball, race, asteroids_destroyed, asteroids_spawned)
                    draw_winner(winner_text, reason, "")
                    break

            if send and len(loots) < 1 and spawn_loots and ballmode == False and race == False:
                spawn_loot(loots, lootTypes)

            if a_send == False and len(asteroids) < 1 and spawn_asteroids_boolean and ballmode == False and race == False:
                a_send = True
                spawn_asteroids(asteroids, a_send)

            if a_send == False and check_a_pos(asteroids) and race:
                a_send = True
                spawn_asteroids_race(asteroids, a_send)

            if red_vampire and red_v_hits >= 2:
                red_v_hits = 0
                red_hp += 1
            if yellow_vampire and yellow_v_hits >= 2:
                yellow_v_hits = 0
                yellow_hp += 1

            if not regeneration_send:
                regeneration_send = True
                reg_hp.start()
            if not c_effect_send:
                c_effect_send = True
                c_effect.start()

            keys_pressed = pygame.key.get_pressed()
            yellow_handle_movement(keys_pressed, yellow, yellow_mspeed, True, yellow_c_mspeed)
            if race and not single_player_survival:
                red_handle_movement(keys_pressed, red, red_mspeed, red_c_mspeed, race)
            elif not race:
                red_handle_movement(keys_pressed, red, red_mspeed, red_c_mspeed, race)
            handle_loots(loots, red, yellow, lootTypes, loot_texts)
            handle_granades(yellow_granades, red_granades, yellow, red, particles, loots, lootTypes)
            handle_bullets(yellow_bullets, red_bullets, yellow, red, loots, lootTypes, yellow_bspeed, red_bspeed, particles, asteroids, red_dd, yellow_dd, yellow_granades, red_granades, yellow_c_bspeed, red_c_bspeed, red_c_ddc, yellow_c_ddc, ballmode, race)
            handle_asteroids(asteroids, red, yellow, loots, lootTypes, particles, yellow_granades, red_granades)

            if ballmode:
                handle_ball(ball, red_bullets, yellow_bullets, red, yellow)

            draw_window(red, yellow, red_bullets, yellow_bullets, red_hp, yellow_hp, coordinates, loots, lootTypes, yellow_shield, red_shield, particles, loot_texts, asteroids, red_dd, yellow_dd, yellow_granades, red_granades, pygame.time.get_ticks() - start_time, turnaument, red_wins, yellow_wins, yellow_c_bullets, red_c_bullets, yellow_c_mines, red_c_mines, ballmode, ball, race, asteroids_destroyed, asteroids_spawned)
    if quit:
        menu()
        if reg_hp.t.is_alive():
            reg_hp.t.cancel()
        if c_effect.t.is_alive():
            c_effect.t.cancel()
    elif not turnaument:
        menu()
        if reg_hp.t.is_alive():
            reg_hp.t.cancel()
        if c_effect.t.is_alive():
            c_effect.t.cancel()
    elif turnaument and yellow_wins < tournament_wins and red_wins < tournament_wins:
        if reg_hp.t.is_alive():
            reg_hp.t.cancel()
        if c_effect.t.is_alive():
            c_effect.t.cancel()
        game(turnaument, red_wins, yellow_wins, ballmode, race)
    else:
        if reg_hp.t.is_alive():
            reg_hp.t.cancel()
        if c_effect.t.is_alive():
            c_effect.t.cancel()
        menu()

if __name__ == "__main__":
    menu()
