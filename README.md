# Space Game
![](https://gitlab.com/orasponka/space-game/-/raw/main/screenshots/logo.png) <br> <br>
Space Game is simple two player game. Idea is destroy opposite player's spaceship. <br>
Game is made with Python and Pygame.

I don't own any of assets, sounds or fonts but i think that i can use them for free on this project. <br> 
If you own something on this repo and i don't have rights to use them please contact me on email (orasponka@gmail.com) and i can remove them if you want.

Based on Tech with Tim [pygame tutorial](https://www.youtube.com/watch?v=jO6qQDNa2UY) but i have made a lot changes myself. <br>
Spaceship assets made by Skorpio and i found them [here.](https://opengameart.org/content/spaceship-fighter-ipod1) <br>
Part of sound effects found on [freesound.org](https://freesound.org) <br>
Checkout credits (that you can found in game) for more information about who have made art that Space Game uses or contact me for more info. <br>

## Contents
- [How to install?](https://gitlab.com/orasponka/space-game#how-to-install)
- [Known problems and bugs](https://gitlab.com/orasponka/space-game#known-problems-and-bugs)
- [Controls](https://gitlab.com/orasponka/space-game#controls)
- [In Game Items](https://gitlab.com/orasponka/space-game#in-game-items)
- [Gamemodes](https://gitlab.com/orasponka/space-game#gamemodes)
- [Every big update explained](https://gitlab.com/orasponka/space-game#every-big-update-explained)
- [Every version explained](https://gitlab.com/orasponka/space-game#every-version-explained)
- [Gallery](https://gitlab.com/orasponka/space-game#gallery)

## How to install?
Make sure that you have installed pygame and python.
### Linux
```
git clone https://gitlab.com/orasponka/space-game.git
cd space-game
python game.py 
```
### Windows
1. Download this repository as zip file.
2. Extract zip file.
3. Run spacegame.bat that's in extracted folder.
## Known problems and bugs
- Playing on different screen sizes changes gameplay.
- Tutorial is okay only with default screen size.
- Space Game's code base is total mess.
- I don't know anything about optimizing games.
- I don't know how to get people testing/playing Space Game.
- I don't know how to correctly license this game.
## Controls
### Player 1
W - Move up <br>
A - Move left <br>
S - Move down <br>
D - Move right <br>
F - Shoot <br>
G - Use mine
### Player 2
Arrow Up - Move up <br>
Arrow Left - Move left <br>
Arrow Down - Move down <br>
Arrow Right - Move Right <br>
Right CTRL / END / Numpad 0 - Shoot <br>
Right Shift / Numpad 1 - Use mine
### Other
F3 (In Game) - Show coordinates <br>
Space (In Menu) - Start Game <br>
ESC (In Menu) - Quit Game <br>
ESC (In Game) - Back to menu <br>
ENTER (In Menu) - Open controls <br>
W and S (In class menu) - Change Yellow's class <br>
Arrows up and down (In class menu) - Change Red's class
Arrow Keys / 1-2 / A D - Change gamemode
## In Game Items
Red - Gives three more health points<br>
Green - Increases bullet speed by 2<br>
Blue - Gives shield for 5 seconds <br>
Yellow - Increases move speed by 2<br>
Purple - Gives double damage for 5 seconds<br>
Orange - Increases bullet speed, movement speed and hp by 1
## Gamemodes
### Single Game
- Normal Gamemode
- Defeat opposite player
- Classes aviable
### Tournament
- Many matches in a row
- Best of five matches win
- Classes aviable
### Space Soccer - Minigame
- Shoot ball into the opponent's goal
- Classes not aviable
### Survival 
- Survive as long as possible in the middle of the asteroids
- Classes not aviable
- Single and two player versions
## Every big update explained
### v1.0 - v1.0.3 
- Normal game mode. 
- Simple menu
- Four different type of items ( bullet speed, movement speed, hp, shield )
- Press enter to open controls in browser 
- When bullets collide they are destroyed
### v1.1 - Tutorial Update
- Tutorial added
- Particles added
- Added text that tell what item player pickupped
### v1.2 - v1.2.2 - Asteroid Update
- Asteroids added
- Added sound for final hit
- Updated tutorial
- Added asteroids to menu
### v1.3 - v1.3.5 - Gameplay Update
- Added new item ( double damage )
- Updated asteroid spawning system
- Asteroid sizes are randomized
- HP ammount changed from 10 to 12
- Old items updated
- Better keybinds
### v1.4 - v1.4.3 - Gameplay Update 2
- Mines added
- Clock added 
- Updated tutorial
- Changed asteroid storm rarity
- Player start positions fixed
- Better keybinds
### v1.5 - v1.5.1 - Tournament Update
- Added tournament mode ( Best of five )
- Audio and gameplay changes
- Added text that tell what killed loser below winner text
### v1.6 - v1.6.6 - Class Update
- Added classes (Tank, Sniper, Engineer, Assasin, Lucky, Mad, Healer, Challenger, Scout, Vampire)
- Bug fixes
### v1.7 - v1.7.10 - Settings Update
- Added settings
- Added text buttons
### v1.8 - Settings Update 2
- Better Settings
### v1.9 - v1.9.6 - Minigame Update
- Added Space Soccer minigame
- Added Credits
- Added potatoas and spaghetti to game
- And a lot minor changes
### v1.10 - v1.10.9 - Survival Update
- Added survival mode, which have two versions ( Single Player and Two Player ) and four difficulties.
- Updated settings.
### v1.11 - v1.11.5 - Content update
- Added custom window size option
- Balanced classes
- Added new loot (Joker that gives 1 Bullet Speed, 1 Movement Speed and 1 HP)
- Small changes to credits
- Added difficulty text in Two Player Survival
- Asteroids are now little weaker in Survival mode
- Changes to survival difficulties
- Default window size changed to 1050x650
- Better menu navigation
- Updates to Space Soccer
## Every version explained
### v1.0 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/ee859e2987efd765cc69c682ee02cea7b6e6462c)
Basicly first version. 
### v1.0.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/88dd70cf868e406e66754f6f905c06ba6df4bf10)
Minor changes and fixes
### v1.0.2 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/c8b441b344a2344b0d17a1cff58e82d9ca1fde4c)
Minor changes and fixes. I don't remember :)
### v1.0.3 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/1c231c4026c97e28502d30ef6d5615300ae642bf)
Added "Press ENTER to open controls in web browser." <br>
Added feature that destroys bullets when they collide. <br>
And minor changes.
### v1.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/a01daf724f7a760c5f6c932626deabf4fca93cb1)
Added tutorial. <br>
Added text that shows up when player pickups loot. <br>
Added particles.
### v1.2 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/6e7a166fb33eb5c785bc001c0bed28a6666f3c4e)
Added asteroids. <br>
And other changes.
### v1.2.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/b158d4cbdf25074feb2ea6e99f3644af6f9edbb5)
Added sound effect for final hit.
### v1.2.2 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/41ab51b494e8c191455eea17001bd16c6e5da714)
Updated tutorial.
### v1.3 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/a75d6d6a0d76ea608d98479f7bfb6ea80f0fa3a6)
Added duoble damage item. <br>
Update for asteroid spawning system. <br>
And other changes.
### v1.3.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/f3b8ab281431e84fa65efc90fab9932f5c9fdbfb)
Asteroid spawning is more random and biger. <br>
Asteroid sizes are randomized. <br>
Bug fixes. <br>
### v1.3.2 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/443279dc3aeba9f104b2658f24412ad6414d5091)
Minor updates. <br>
Move Speed and bullet speed items effect boosted.
### v1.3.3 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/991ad601f581ff2204ac09c1e20d971ff8d0b61f)
Added more keys for red player shooting. <br>
Updated tutorial for new keybinds
### v1.3.4 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/cdaa0d5e023b2cfaa6272313cb14dd5f8fb9584a)
Added asteroid storms. <br>
Asteroid break particle effect color randomized. <br>
HP changed to 12 from 10.
### v1.3.5 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/af78585ccc633bb3780cba5fffe1761e7147bd7a)
Minor changes and items balanced.
### v1.4 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/e2b89588fcf9650c92e7499ead4ffca089927991)
Added mines. <br>
Added clock. <br>
Updated tutorial <br>
Changed asteroid storm rarity <br>
And other changes
### v1.4.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/748873d433e8551edb1c49154f036e13f4ea09ff)
Bug fixes.
### v1.4.2 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/5ad3d2888c11319f15d647ec75e3d3635ac4a9ed)
Player start positions fixed.
### v.1.4.3 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/eefcbb674d6c87eb116c384d8eac9e335e25ebcd)
Improved keybinds for red player. <br>
Updated tutorial <br>
And other changes <br>
### v1.5 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/44a2119578aca6a07eb75770d526ddbbf52f6900)
Added tournament mode (best of five mode). <br>
And other audio and gameplay changes.
### v1.5.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/2d525b4f32d75e135d725ff09de14c9afc990d1d)
Added text that tell what killed loser below winner text. <br>
And other minor changes.
### v1.6 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/2409bf139cf6d0a3d4904aa391d612ee307ee739)
Added classes (Tank, Sniper, Engineer, Assasin, Lucky, Mad, Healer and Challenger).
### v1.6.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/dc03c41f8b8f01c16faedbce64f67b01afc76874)
Classes balanced.
### v1.6.2 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/fefeb071950a2ca9b9bfde575c2463c619042bd8)
Classes balanced.
### v1.6.3 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/dae258a186471f75896a25653e87915435ce2cd2)
Bug fixes
### v1.6.4 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/c5f81db7a671148721d0558b1608b90861560447)
Added two new classes. (Scout, Vampire) <br>
And other minor changes.
### v1.6.5 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/45428f62f656b4784644c5111a9b579d70dea9a3)
Big changes to classes.
### v1.6.6 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/d82eec3e44f6f7cdd45774263f90920c4c4616d9)
Bug fixes and other minor changes.
### v1.7 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/7de519fda29d13798c82aa032d89e9c2365ba0be)
Added settings. <br>
And other changes.
### v1.7.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/28c2e7542b722cdf1090be44b95cdce9cc11b229)
Bug fixes
### v1.7.2 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/125b1b5674bdddb66394c5a684b9e8bdda6dee22)
Optimizing game for diffirent display sizes.
### v1.7.3 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/2de5487f7433c1a21bd6a15e052d3ce59a0cb75e)
Added settings for appearance. (Can change player color) <br>
And other minor changes.
### v1.7.4 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/10943350cc5705ecd0002c6aec30c784811e4c63)
Bug fixes.
### v1.7.5 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/4e89ef5b7f15479ad4b54e88aa7f76683245ae1b)
Added support for many different display sizes. 
### v1.7.6 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/0104865c52b7db7dd7e713d206ed9b9c21e2d678)
Added hover color for buttons. <br>
Added small audio settings. <br>
And other updates and fixes.
### v1.7.7 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/b32dbf205dce34fa0417a6453faa9fb53726e507)
Updated menu. 
### v1.7.8 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/bc5d08b623aa1fed6ea39b7c018a0e8e062463b4)
Minor changes to audio.
### v1.7.9 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/9caf8d5e48884edcbf81ac17868a2455cf89269c)
Better audio settings.
### v1.7.10 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/d1b74ca8eea310a3203cc0b70ea0ab8d19d77fce)
Bug fixes. 
### v1.8 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/e93500107eed2b80c0bbd3697208873a35c3cfb1)
Better settings.
### v1.9 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/3861c84124cf612a1a5cf6966994a6e592edc73c)
Added Space Soccer game mode. <br>
And other minor changes
### v1.9.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/676ab4a043807d76207ad7f76bcf6a6e24dfe24e)
Minor Changes to Space Soccer.
### v1.9.2 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/2a2b45ec860b66691333ef12ed92cc543104ca5b)
Bug fixes.
### v1.9.3 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/f5b843969e44c53229ef79f0ef77be6f78e96068)
Added credits. <br>
Added potatoes to game.
### v1.9.4 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/8818564477820b00b1f07f31a7aa9d70e5962692)
Minor changes to settings and space soccer.
### v1.9.5 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/bbb9faeb988ce89a720b871422b8fa44cbd4152d)
Minor changes to credits.
### v1.9.6 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/9d29f29905b38a6d38c95172a0ed9044e75b6a3a)
Added spaghetti and other minor changes.
### v1.10 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/b65cdc72a3fc57a55569f89d4a38f959a5cc18ee)
Added survival mode 
### v1.10.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/be56071f182b3d83c2e19acf0e33e1e9686b3439)
Updated survival mode 
### v1.10.2: [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/49bc7fc636a534a659c232025ee303e29a1d3a91)
Updated survival mode 
### v1.10.3 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/c19653b181531c2d7a000655d4195938057a1c65)
Bug fixes 
### v1.10.4 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/905d130385354b7b5df8e454ce1a70671809fdb3)
 A lot small/medium size changes 
### v1.10.5 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/0d5a0947c21b2105ba70d33b89cdac0b5be4fece)
Added asteroid counter and minor changes to survival mode 
### v1.10.6 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/9039998547f6f9600dd19c3984b7097b8d5f03e7)
Added difficulties to survival mode and updated settings and more 
### v1.10.7 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/5ddafe717a68422a5cbdb5840bd712909730cb81)
Bug fix
### v1.10.8 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/8e0d2f77030fccb38003f4adbf199ab4d7006a2c)
Updated settings
### v1.10.9 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/dd1b20bdeb88699400bb1e0140fb12516beb3f42)
- Optimized code
### v1.11 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/9e95a7615fe905f1f02c4d1862847c8a691e91b8)
- Added custom window size option into settings
- Balanced classes
- Default window size changed to 1050x650
### v1.11.1 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/438557f0badc7ac603d984c3ac50f0c46a5b4e6d)
- Added new loot (Joker that gives 1 Bullet Speed, 1 Movement Speed and 1 HP)
- Small changes to credits
- Added difficulty text in Two Player Survival
- Asteroids are now little weaker in Survival mode
- Changes to survival difficulties
### v1.11.2 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/628999ce3bba1f25c65d50ebebb6eda583fb9970)
- Better class menu
### v1.11.3 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/705f5b339c6dd7ff2b5d583291c572534779e24d)
- Better menu navigation
- Typos corrected
- Two Player survival mode enabled by default.
- Small changes to classes (Sniper, Assassin)
### v1.11.4 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/7a4cf7a684d6f4d66a0411f85f230e2f9f6d10bf)
- Better Space Soccer
- Added buttons for Tutorial and Classes menu (instead of old texts)
- Bug fixes
### v1.11.5 : [Browse Files](https://gitlab.com/orasponka/space-game/-/tree/a66a2d02657db4244af118dceb3fe9234d59163a)
- Bug fix
### Menu Screen
![](https://gitlab.com/orasponka/space-game/-/raw/main/screenshots/menu3.png)
### Class Menu
![](https://gitlab.com/orasponka/space-game/-/raw/main/screenshots/classes2.png)
### Tutorial
![](https://gitlab.com/orasponka/space-game/-/raw/main/screenshots/tutorial2.png)
### Gameplay
![](https://gitlab.com/orasponka/space-game/-/raw/main/screenshots/gameplay2.png)
### Space Soccer Gameplay
![](https://gitlab.com/orasponka/space-game/-/raw/main/screenshots/spacesoccer2.png)
### Settings
![](https://gitlab.com/orasponka/space-game/-/raw/main/screenshots/settings3.png)
